module.exports = {
  env: {
    browser: true,
    es2021: true,
    es6: true,
  },
  extends: [
    'airbnb-base',
  ],
  parser: 'babel-eslint', // 解析器，默认使用Espree
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module', // 指定来源的类型，"script" (默认) 或 "module"（如果你的代码是 ECMAScript 模块)
  },
  rules: {
    'no-console': 0,
  },
};
