## [1.0.11](https://gitee.com/yanhuakang/webpack-demos/compare/v1.0.10...v1.0.11) (2022-04-14)


### ✨ Features | 新功能

* 1.0.10-1 ([3544d95](https://gitee.com/yanhuakang/webpack-demos/commit/3544d95))
* 1.0.10-2 ([8d24600](https://gitee.com/yanhuakang/webpack-demos/commit/8d24600))
* 1.0.10-3 ([1f24579](https://gitee.com/yanhuakang/webpack-demos/commit/1f24579))



## [1.0.10](https://gitee.com/yanhuakang/webpack-demos/compare/v1.0.9...v1.0.10) (2022-04-14)


### ✨ Features | 新功能

* 1.0.9-1 ([2548176](https://gitee.com/yanhuakang/webpack-demos/commit/2548176))



## [1.0.9](https://gitee.com/yanhuakang/webpack-demos/compare/v1.0.8...v1.0.9) (2022-04-14)


### ✨ Features | 新功能

* 1.0.8-1 ([1e4762d](https://gitee.com/yanhuakang/webpack-demos/commit/1e4762d))



## [1.0.8](https://gitee.com/yanhuakang/webpack-demos/compare/v1.0.4...v1.0.8) (2022-04-14)


### ✨ Features | 新功能

* 1.0.5 ([5a9206c](https://gitee.com/yanhuakang/webpack-demos/commit/5a9206c))
* 1.0.6 ([cba791d](https://gitee.com/yanhuakang/webpack-demos/commit/cba791d))
* 1.0.6 ([27eae24](https://gitee.com/yanhuakang/webpack-demos/commit/27eae24))
* 1.0.6-2 ([04fc461](https://gitee.com/yanhuakang/webpack-demos/commit/04fc461))
* 1.0.7-1 ([6b3d0f8](https://gitee.com/yanhuakang/webpack-demos/commit/6b3d0f8))



## [1.0.4](https://gitee.com/yanhuakang/webpack-demos/compare/fbde054...v1.0.4) (2022-04-14)


### ⏪ Reverts | 回退

* revert ([099e8c2](https://gitee.com/yanhuakang/webpack-demos/commit/099e8c2))


### ⚡ Performance Improvements | 性能优化

* perf ([9ff3ec2](https://gitee.com/yanhuakang/webpack-demos/commit/9ff3ec2))
* test ([d0da2e9](https://gitee.com/yanhuakang/webpack-demos/commit/d0da2e9))


### ✨ Features | 新功能

* @babel/polyfill ([66e5281](https://gitee.com/yanhuakang/webpack-demos/commit/66e5281))
* 1.0.3 ([0c56435](https://gitee.com/yanhuakang/webpack-demos/commit/0c56435))
* 1.0.3 ([3d43035](https://gitee.com/yanhuakang/webpack-demos/commit/3d43035))
* changelog-option.js ([ec6b8d2](https://gitee.com/yanhuakang/webpack-demos/commit/ec6b8d2))
* eslint ([85fa907](https://gitee.com/yanhuakang/webpack-demos/commit/85fa907))
* eslint ([938d8f8](https://gitee.com/yanhuakang/webpack-demos/commit/938d8f8))
* feat ([cc11474](https://gitee.com/yanhuakang/webpack-demos/commit/cc11474))
* husky ([2818086](https://gitee.com/yanhuakang/webpack-demos/commit/2818086))
* postcss、css modules ([bbde461](https://gitee.com/yanhuakang/webpack-demos/commit/bbde461))
* Service Worker ([eb0e69c](https://gitee.com/yanhuakang/webpack-demos/commit/eb0e69c))
* Service Worker ([fdc7d4b](https://gitee.com/yanhuakang/webpack-demos/commit/fdc7d4b))
* test conventional-changelog ([5df8e06](https://gitee.com/yanhuakang/webpack-demos/commit/5df8e06))
* test husky ([b4f6b94](https://gitee.com/yanhuakang/webpack-demos/commit/b4f6b94))
* useBuiltIns ([374761d](https://gitee.com/yanhuakang/webpack-demos/commit/374761d))
* webpack 构建 library ([098b073](https://gitee.com/yanhuakang/webpack-demos/commit/098b073))
* webpack基础2-asset module 资源模块 ([009b216](https://gitee.com/yanhuakang/webpack-demos/commit/009b216))
* webpack基础2-asset module 资源模块 ([90c267f](https://gitee.com/yanhuakang/webpack-demos/commit/90c267f))
* webpack基础一 ([fbde054](https://gitee.com/yanhuakang/webpack-demos/commit/fbde054))
* webpack基础篇（七）：环境变量 ([b8cd327](https://gitee.com/yanhuakang/webpack-demos/commit/b8cd327))
* webpack基础篇（三）：管理资源（image、css、fonts、csv、json5） ([d0e1283](https://gitee.com/yanhuakang/webpack-demos/commit/d0e1283))
* webpack基础篇（五）：代码分离（Code Splitting） ([7ceefc2](https://gitee.com/yanhuakang/webpack-demos/commit/7ceefc2))
* webpack基础篇（六）：缓存 caching（hash） ([03364bc](https://gitee.com/yanhuakang/webpack-demos/commit/03364bc))
* webpack基础篇（四）：babel-loader ([b8c7fa4](https://gitee.com/yanhuakang/webpack-demos/commit/b8c7fa4))
* webpack基础篇（四）：babel-loader ([da28dc9](https://gitee.com/yanhuakang/webpack-demos/commit/da28dc9))
* 打包结果分析 - 依赖图(dependency graph) ([63b00bb](https://gitee.com/yanhuakang/webpack-demos/commit/63b00bb))
* 新功能（feature） ([9c080ea](https://gitee.com/yanhuakang/webpack-demos/commit/9c080ea))
* 新功能（feature） ([accf35e](https://gitee.com/yanhuakang/webpack-demos/commit/accf35e))
* 模块联邦 ([b53826d](https://gitee.com/yanhuakang/webpack-demos/commit/b53826d))
* 模块联邦 ([c5661b3](https://gitee.com/yanhuakang/webpack-demos/commit/c5661b3))
* 模块解析（Module Resolution） ([9375698](https://gitee.com/yanhuakang/webpack-demos/commit/9375698))


### 🐛 Bug Fixes | Bug 修复

* fix ([68c67ff](https://gitee.com/yanhuakang/webpack-demos/commit/68c67ff))
* 修复xxx ([5b52dfe](https://gitee.com/yanhuakang/webpack-demos/commit/5b52dfe))



