const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'production',

  devtool: 'hidden-source-map',

  output: {
    clean: true,
  },

  plugins: [
    new HtmlWebpackPlugin(),
  ],
};
