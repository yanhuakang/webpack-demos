import _ from 'lodash';

export const add = (x, y) => _.add(x, y);

export const subtract = (x, y) => x - y;
