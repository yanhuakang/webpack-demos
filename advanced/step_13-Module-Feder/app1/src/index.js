// import Home from './Home';
//
// const home = document.createElement('h1');
// home.textContent = '这里是 App1 的 Home模块';
// document.body.appendChild(home);
// document.body.innerHTML += Home(5);

import Home from './Home';

/**
 *  需要异步导入
 *  App2 为 remotes 中定义的资源别名
 *  ./Header 为 APP2 exposes 定义的 ./Header
 */

// eslint-disable-next-line import/no-unresolved
import('App2/Header').then(({ default: Header }) => {
  const body = document.createElement('div');
  body.appendChild(Header());
  document.body.appendChild(body);

  const home = document.createElement('h1');
  home.textContent = '这里是 App1 的 Home 模块：';
  document.body.appendChild(home);
  document.body.innerHTML += Home(5);
});
