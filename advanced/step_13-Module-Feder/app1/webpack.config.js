const HtmlWebpackPlugin = require('html-webpack-plugin');
const { ModuleFederationPlugin } = require('webpack').container;

module.exports = {
  mode: 'production',
  entry: './src/index.js',
  devServer: {
    port: '3000',
  },
  plugins: [
    new HtmlWebpackPlugin(),

    new ModuleFederationPlugin({
      // 模块联邦名字，提供给其他模块使用
      name: 'app1',
      // 提供给外部访问的资源入口
      filename: 'App1RemoteEntry.js',
      // 引用的外部资源列表
      remotes: {
        /**
         *  App2 引用其他应用模块的资源别名
         *  app2 是 APP2 的模块联邦名字
         *  http://localhost:3001 是 APP2 运行的地址
         *  App2RemoteEntry.js 是 APP2 提供的外部访问的资源名字
         *  可以访问到 APP2 通过 exposes 暴露给外部的资源
         */
        App2: 'app2@http://localhost:3001/App2RemoteEntry.js',
      },
      // 暴露给外部的资源列表
      exposes: {},
      // 共享模块，如lodash
      shared: {},
    }),
  ],
};
