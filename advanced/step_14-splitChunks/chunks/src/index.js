import moment from 'moment';

import(/* webpackChunkName: 'lodash-index' */ 'lodash').then(({ default: lodash }) => {
  lodash.findIndex([1, 2, 3]);
});

console.log('index', moment().format('YYYY-MM-DD HH:mm:ss'));

console.log('My index is running');
