import moment from 'moment';

import(/* webpackChunkName: 'lodash-main-1' */ 'lodash').then(({ default: lodash }) => {
  lodash.findIndex([1, 2, 3]);
});

console.log('main1', moment().format('YYYY-MM-DD HH:mm:ss'));

console.log('My main1 is running');
