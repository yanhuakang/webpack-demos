const HtmlWebpackPlugin = require('html-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

module.exports = {
  mode: 'development',

  entry: {
    main1: './src/main-1.js',
    main2: './src/main-2.js',
  },

  output: {
    clean: true,
    filename: 'js/[name].[contenthash].js',
  },

  plugins: [
    new HtmlWebpackPlugin(),
    new BundleAnalyzerPlugin(),
  ],

  optimization: {
    splitChunks: {
      // chunks: 'async',
      // chunks: 'initial',
      chunks: 'all',
      // minChunks: 3,
    },
  },
};
