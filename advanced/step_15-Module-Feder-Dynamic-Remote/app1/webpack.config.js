const HtmlWebpackPlugin = require('html-webpack-plugin');
const { ModuleFederationPlugin } = require('webpack').container;

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    clean: true,
  },
  devtool: false,
  devServer: {
    port: '3000',
    client: {
      logging: 'none',
    },
  },
  plugins: [
    new HtmlWebpackPlugin(),

    new ModuleFederationPlugin({
      // 模块联邦名字，提供给其他模块使用
      name: 'app1',
      // 提供给外部访问的资源入口
      filename: 'App1RemoteEntry.js',
      // 引用的外部资源列表
      remotes: {
        /**
         *  App2 引用其他应用模块的资源别名
         *  app2 是 APP2 的模块联邦名字
         *  http://localhost:3001 是 APP2 运行的地址
         *  App2RemoteEntry.js 是 APP2 提供的外部访问的资源名字
         *  可以访问到 APP2 通过 exposes 暴露给外部的资源
         */
        App2: 'app2@http://localhost:3001/App2RemoteEntry.js',
      },
      // 暴露给外部的资源列表
      exposes: {},
      // 共享模块，如lodash
      shared: {},
    }),

    new ModuleFederationPlugin({
      name: 'app1',
      remotes: {
        App3: `promise new Promise(resolve => {
          // TODO
          // 可以在这里设计和实现你的 模块联邦 版本化，这里简单的从URL获取version
          const urlParams = new URLSearchParams(window.location.search)
          const version = urlParams.get('app2VersionParam')
          console.log('version', version);
          const remoteUrlWithVersion = 'http://localhost:3001/' + version + '/App2RemoteEntry.js'
          console.log('remoteUrlWithVersion', remoteUrlWithVersion);
          const script = document.createElement('script')
          script.src = remoteUrlWithVersion
          script.onload = () => {
            // 注入的脚本已经加载并在window上可用
            // 我们现在可以解析这个Promise
            const proxy = {
              get: (request) => {
                console.log('request', request);
                return window.app2.get(request)
              },
              init: (arg) => {
                try {
                  console.log('arg', arg);
                  return window.app2.init(arg)
                } catch(e) {
                  console.log('remote container already initialized')
                }
              }
            }
            resolve(proxy)
          }
          // 将script的src设置为版本化的remoteEntry.js
          document.head.appendChild(script);
        })
       `,
      },
    }),
  ],
};
