const files = require.context('./modules', false, /.js$/);

const modules = {};
console.log('files：', files);

files.keys().forEach((key) => {
  console.log('key：', key);
  modules[key.replace(/(\.\/|\.js)/g, '')] = files(key).default;
});

console.log('modules：', modules);
console.log('resolve：', files.resolve('./a.js'));
console.log('files.id：', files.id);

// files： ƒ n(e){var r=a(e);return t(r)}
// key： ./a.js
// key： ./b.js
// key： ./c.js
// modules： {a: ƒ, b: ƒ, c: ƒ}
// resolve： 84
// files.id： 635
