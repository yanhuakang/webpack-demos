const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'production',

  output: {
    clean: true,
    filename: 'js/[name].[contenthash].js',
  },

  devServer: {
    open: true,
    client: {
      logging: 'none',
    },
  },

  plugins: [
    new HtmlWebpackPlugin(),
  ],
};
