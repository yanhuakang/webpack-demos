import axios from 'axios';

const getData = () => {
  axios.get('/api/hello')
    .then(({ data }) => {
      console.log(data);
    });
};

export default getData;
