const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'production',

  devtool: 'eval-cheap-source-map',

  output: {
    clean: true,
  },

  devServer: {
    client: {
      overlay: false,
      logging: 'none',
    },
    proxy: {
      '/api': {
        target: 'http://localhost:3100',
        changeOrigin: true,
        // pathRewrite: { '^/api': '' },
        /**
         changeOrigin、pathRewrite 不生效？

         有些人可能会通过浏览器 F12 查看 request Header 的 Host，发现它无论怎么修改 changeOrigin，它均为前端服务器地址（http://localhost:8080）。于是就认为 changeOrigin: true 无效。其实不然。而且，设置已经生效了，只是浏览器不会直观地显示给你。

         你需要通过后端进行 request.getHeader("Host") 打印，你就能发现区别了。

         当不设置 changeOrigin 的时候，后端输出 http://localhost:8080

         当设置 changeOrigin: true 的时候，后端输出 http://localhost:3000

         这大约是因为，浏览器只是将第一层请求显示给你，也就是发给代理服务器的请求，而修改 Host 的工作是代理服务器做的，浏览器当然不会显示 Host 已经被修改的状态！
         同理 pathRewrite 也是这个道理
         */
      },
    },
  },

  plugins: [
    new HtmlWebpackPlugin(),
  ],
};
