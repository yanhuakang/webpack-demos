module.exports = {
  env: { // 指定脚本的运行环境。每种环境都有一组特定的预定义全局变量。
    browser: true, // 运行在浏览器
    es2021: true, // 支持es2021
  },
  extends: [ // 使用的外部代码格式化配置文件
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12, // ecmaVersion 用来指定支持的 ECMAScript 版本 。默认为 5，即仅支持es5
    sourceType: 'module',
  },
  globals: {}, // 脚本在执行期间访问的额外的全局变量
  rules: {
    'no-console': 0,
  }, // 启用的规则及其各自的错误级别。0(off)  1(warning)  2(error)
};
