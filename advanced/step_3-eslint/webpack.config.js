const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',

  output: {
    clean: true,
  },

  plugins: [
    new HtmlWebpackPlugin(),
  ],

  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node-modules/, // 排除编译 node_modules
        use: ['eslint-loader'],
      },
    ],
  },
};
