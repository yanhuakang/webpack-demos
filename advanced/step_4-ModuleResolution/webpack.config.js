const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',

  output: {
    clean: true,
  },

  plugins: [
    new HtmlWebpackPlugin(),
  ],

  // 外部扩展依赖
  externals: {
    jquery: 'jQuery',
  },

  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
    extensions: ['.js', '.json', '.jsx'],
  },
};
