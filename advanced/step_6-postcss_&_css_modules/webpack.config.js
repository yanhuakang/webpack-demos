const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const postcssNested = require('postcss-nested');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'development',
  output: {
    clean: true, // 在生成文件之前清空 output 目录
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/[name].[hash].css', // 定义抽离的入口文件的文件名
      chunkFilename: 'css/[name].[hash].css', // 定义非入口块文件的名称。
    }),

    new HtmlWebpackPlugin({
      template: './index.html',
    }),
  ],

  module: {
    rules: [
      {
        test: /\.(css|less)$/i,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: {
                mode: 'local',
                // 样式名规则配置
                localIdentName: '[name]__[local]--[hash:base64:5]',
              },
            },
          },
          'less-loader',
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: { // 也可使用 PostCSS 配置文件 postcss.config.js 代替
                plugins: [
                  autoprefixer, // 为CSS 规则添加前缀
                  postcssNested, // 为CSS提供编写嵌套的样式语法
                ],
              },
            },
          },
        ],
      },
    ],
  },
};
