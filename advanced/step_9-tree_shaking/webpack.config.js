const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'production',

  output: {
    clean: true,
  },

  plugins: [
    new HtmlWebpackPlugin(),
  ],

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
    ],
  },

  optimization: {
    usedExports: true,
  },
};
