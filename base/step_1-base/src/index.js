// index.js
import { add, reduce } from './math';

const result1 = add(11, 22);
const result2 = reduce(11, 22);

console.log(result1);
console.log(result2);
console.log(result2);
