const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, './assets'),
    filename: '[name].[contenthash].js',
    clean: true,
  },
  devServer: {
    static: './assets',
    port: '3000',
    open: true,
    client: {
      logging: 'none',
    },
  },
  watch: true,
  devtool: 'eval-source-map',
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html',
      filename: 'index.html',
      inject: 'body',
      title: 'Webpack App',
    }),
  ],
};
