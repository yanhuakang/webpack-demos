// src/index.js
import icon from './static/icon.png';

import empty from './static/empty.svg';

import helloWebpack from './static/hello-webapck.txt';

import figure from './static/figure.jpeg';

const iconImg = document.createElement('img');
iconImg.style.cssText = 'width: 200px;';
iconImg.src = icon;
document.body.appendChild(iconImg);
const emptyImg = document.createElement('img');
emptyImg.style.cssText = 'width: 200px;';
emptyImg.src = empty;
document.body.appendChild(emptyImg);
const text = document.createElement('p');
text.style.cssText = 'width: 200px;height: 200px;background-color: pink;';
text.innerText = helloWebpack;
document.body.appendChild(text);
const figureImg = document.createElement('img');
figureImg.style.cssText = 'width: 200px;';
figureImg.src = figure;
document.body.appendChild(figureImg);
