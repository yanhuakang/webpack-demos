/**
 * 当设置 'useBuiltIns: 'usage' 时，polyfills 会在需要时自动导入。
 * 请删除 '@babel/polyfill' 的直接导入或使用 'useBuiltIns: 'entry' 代替。
 */
// import '@babel/polyfill';
const array = [1, 2, 3, 4, 5, 6];
console.log(array.includes((item) => item > 2));
const getData = () => new Promise((resolve) => {
  console.log(111);
  resolve({});
});
const data = async () => {
  console.log(await getData());
};
data();
