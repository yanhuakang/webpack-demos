const add = (x, y) => x + y;
const reduce = (x, y) => x - y;

export {
  add,
  reduce,
};
