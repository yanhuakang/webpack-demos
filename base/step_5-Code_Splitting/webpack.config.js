const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',

  entry: {
    main1: {
      import: './src/main-1.js',
      dependOn: 'common_lodash',
      // filename: "js/[name].[contenthash].js"
    },
    main2: {
      import: './src/main-2.js',
      dependOn: 'common_lodash',
      // filename: "js/[name].[contenthash].js"
    },
    common_lodash: {
      import: 'lodash',
      // filename: "js/[name].[contenthash].js"
    },
  },

  output: {
    clean: true,
    filename: 'js/[name].[contenthash].js',
  },

  plugins: [
    new HtmlWebpackPlugin(),
  ],

  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },

};
