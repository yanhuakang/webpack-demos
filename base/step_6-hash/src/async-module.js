const getAsyncData = () => {
  const button = document.createElement('button');
  button.textContent = '点击执行加法运算';
  button.addEventListener('click', () => {
    // webpackPrefetch: true 在动态引入时开始预获取
    import(/* webpackChunkName: 'math', webpackPrefetch: true,  */ './math').then(({ add }) => {
      console.log(add(4, 5));
    });
  });
  document.body.appendChild(button);
};

export default getAsyncData;
