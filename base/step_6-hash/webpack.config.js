const HtmlWebpackPlugin = require('html-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');

module.exports = {
  mode: 'development',

  entry: {
    main1: {
      import: './src/main-1.js',
      dependOn: 'common_lodash',
      // filename: "js/[name].[contenthash].js"
    },
    main2: {
      import: './src/main-2.js',
      dependOn: 'common_lodash',
      // filename: "js/[name].[contenthash].js"
    },
    common_lodash: {
      import: 'lodash',
      // filename: "js/[name].[contenthash].js"
    },
  },

  output: {
    publicPath: 'localhost:8080/',
    clean: true,
    filename: 'js/[name].[contenthash].js',
  },

  plugins: [
    new HtmlWebpackPlugin(),
    new WebpackManifestPlugin(),
  ],

  optimization: {
    // runtimeChunk: 'single',
    // moduleIds: 'deterministic',
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  },

};
