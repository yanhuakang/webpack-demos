## 一. 安装webapck

在项目中建议将webpack安装至当前项目而不是全局，这样避免你的小伙伴没有安装或者安装了其他版本的webapck

1. 新建项目`webpack-test`，生成`package.json`文件

```bash
npm init -y
```

2. 安装webpack

```bash
npm i webpack webpack-cli -D
```

3. 检查`webpack`安装版本

```bash
webpack -v
```

4. 测试一下

`webpack-test`项目中新建`src`，`src`中新建`test.js`、`index.js`

```js
// test.js
export function add (x, y) {
  return x + y
}

export function reduce (x, y) {
  return x - y
}
```

```js
// index.js
import { add, reduce } from './test.js'

const result1 = add(11, 22)
const result2 = reduce(11, 22)

console.log(result1);
console.log(result2);
```



执行命令 `webapck`，它会默认找`./src/index.js`作为入口文件

![image-20211129142107538](/Users/HuaJi/Library/Application Support/typora-user-images/image-20211129142107538.png)

你可以看到已经打包出一个`dist/main.js`



## 二. 自定义webpack配置

执行`webpack --help`可以看到`webapck`有很多命令可以使用，比如刚才执行的`webpack`你可以使用这样一个命令行得到相同结果，当然你要先删除`dist`

```bash
webpack --entry ./src/index.js --mode production
```

也会得到`dist/main.js`打包产物

但是这样使用命令行不方便、不直观也不能保存一些其他配置

我们可以使用webpack configuration进行配置

1. 新建`webpack.config.js`

```js
// webpack.config.js
const path = require('path')
module.exports = {
  entry: './src/index.js', // 入口文件

	// 出口配置
  output: {
    filename: 'bundle.js', // 出口文件名
    // 绝对路径
    path: path.resolve(__dirname, './dist') // path.resolve(__dirname) 表示获取当前文件的物理路径，即绝对路径
  },

  mode: 'none' // 打包模式
}
```

2. 执行`webpack`会默认找到`webpack.config.js`配置。可以看到会打包出`dist/bundle.js`，并且内容与`dist/main.js`内容并不一致，而且`dist/main.js`还存在。我们稍后说这两个问题。
3. 在项目根目录下新建`index.html`，并加载`dist/bundle.js`，使用编辑器内置的`server`或者直接在浏览器加载`index.html`可以在控制台看到打印结果`33` `-11`，表明webpack打包成功。



## 三. 自动引入资源

[html-webpack-plugin](https://webpack.docschina.org/plugins/html-webpack-plugin/)

1. 上面我们手动写了`index.html`加载`dist/bundle.js`，那如何自动生成`index.html`并引入打包资源呢？
2. `npm install --save-dev html-webpack-plugin`，让该插件为我们生成一个 HTML 文件

在`webpack.config.js`引入并在`plugins`中实例化它

```js
// webpack.config.js
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  // ...
  plugins: [
    new HtmlWebpackPlugin()
  ]
}
```

3. 执行`webpack`，可以看到`dist/index.html`，在浏览器打开控制台中可以看到可以正常打印结果`33` `-11`。但是目前这个文件和我们前面建的`index.html`还没有什么关联。那我们可以让他们关联起来，或者说通过配置让`HtmlWebpackPlugin`基于我们的`index.html`生成文件

4. 配置`html-webpack-plugin` `option`

```js
// webpack.config.js
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  // ...
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html', // 模板
      filename: 'app.html', // 文件名称
      inject: 'body' // 定义生成的script所在的位置
    })
  ]
}
```

5. 修改`index.html`

```html
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>webpack-自动引入资源-HtmlWebpackPlugin</title>
</head>
<body>
<h1>Hello world</h1>
</body>
</html>
```

6. 执行`webpack`，可以看到`dist/app.html`生成了，并且内容也是我们想要的，在浏览器打开可以正常打印结果`33` `-11`



## 四. 清理dist

我们执行`webpack`后发现我们之前生成的一些文件还在，但事实上我们并不需要他们了，那我们该怎么在生成新的dist前清除掉旧的打包资源呢

只需要简单的配置即可。在`output`中配置`clean: true`即可

```js
// webpack.config.js
module.exports = {
  output: {
    clean: true, // 在生成文件之前清空 output 目录
  }
}
```

执行`webpack`，可以看到打包后只有`app.html`和`bundle.js`，旧文件已经被清除了，`index.html`访问也是正常的



## 五. mode选项

通过选择 `development`, `production` 或 `none` 之中的一个，来设置 `mode` 参数，你可以启用 webpack 内置在相应环境下的优化。其默认值为 `production`。

```javascript
// webpack.config.js
module.exports = {
  mode: 'production',
};
```



## 六. SourceMap

SourceMap能帮助我们在浏览器中调试时看到源码，以帮助我们精准的定位问题代码位置，你只需要在`webpack.config.js`配置`devtool: 'inline-source-map'`即可开启



## 七. watch mode

[watch](https://webpack.docschina.org/configuration/watch/)监听文件变化，当它们修改后会重新编译

```bash
webpack --wacth
```

或者

```javascript
// webpack.config.js
module.exports = {
  // ...
  watch: true,
};
```



## 八. webpack-dev-server

watch 的唯一的缺点是，为了看到修改后的实际效果，我们需要刷新浏览器。如果能够自动刷新浏览器就更好了，因此接下来我们会尝试通过 `webpack-dev-server` 实现此功能。

```bash
npm install --save-dev webpack-dev-server
```

`webpack-dev-server` 为我们提供了一个基本的 web server，并且具有 live reloading(实时重新加载) 功能。

```js
// webpack.config.js
module.exports = {
  devServer: {
    static: './dist',
    port: 9000, // 定义端口
    open: true // 自动打开浏览器
  },
};
```

以上配置告知 `webpack-dev-server`，将 `dist` 目录下的文件 serve 到 `localhost:9000` 下。

打开控制台可以看到

`Hot Module Replacement`热模块更新替换生效了

`Live Reloading`实时更新生效了

即热更新生效了

**Warning**

`webpack-dev-server`在编译之后不会写入到任何输出文件。而是将 `bundle` 文件保留在内存中，然后将它们 `serve` 到 `server` 中，就好像它们是挂载在 `server` 根路径上的真实文件一

这样会提高我们的开发效率，也会提高`webpack`的编译效率



## 九. 资源模块

目前为止，我们的项目可以在控制台上显示"Hello world"。现在我们尝试混合一些其他资源，比如`images`，看看webpack如何处理。

在webpack出现之前，前端开发人员会使用`grunt`和`gulp`等工具来处理资源，并将它们从`/src`文件夹移动到`/dist`或`/build`目录中。

`webpack`最出色的功能之一就是，除了引入`JavaScript`，还可以内置的资源模块`asset module`引入任何其他类型的文件。

资源模块(asset module)是一种模块类型，它允许我们应用`Webpack`来打包其他资源文件（字体，图标等）而无需配置额外 loader。

在 **webpack 5** ***之前***，通常使用：

- [`raw-loader`](https://v4.webpack.js.org/loaders/raw-loader/) 将文件导入为字符串
- [`url-loader`](https://v4.webpack.js.org/loaders/url-loader/) 将文件作为 data URI 内联到 bundle 中
- [`file-loader`](https://v4.webpack.js.org/loaders/file-loader/) 将文件发送到输出目录

资源模块类型(asset module type)，通过添加 4 种新的模块类型，来**替换**所有这些 loader：

- `asset/resource` 发送一个单独的文件并导出 URL。之前通过使用 `file-loader` 实现。
- `asset/inline` 导出一个资源的 data URI。之前通过使用 `url-loader` 实现。
- `asset/source` 导出资源的源代码。之前通过使用 `raw-loader` 实现。
- `asset` 在导出一个 data URI 和发送一个单独的文件之间自动选择。之前通过使用 `url-loader`，并且配置资源体积限制实现。

### 1. asset/resource

修改 `webpack.config.js` 配置：

```js
module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource'
      }
    ]
  }
```

`index.js` 引入图片资源

```js
// src/index.js
import icon from './assets/icon.png'
const iconImg = document.createElement('img')
iconImg.style.cssText = 'width: 200px;'
iconImg.src = icon
document.body.appendChild(iconImg)
```

执行`webpack`可以在`dist`中可以看到有一个`hash.png`文件出现，即为`asset/resource` 发送一个单独的文件

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211129194206402.png" alt="image-20211129194206402" style="zoom:50%;" />

执行`npx webpack-dev-server`打开`index.html`可以看到图片已经出现，打开控制台查看elements可以看到改img标签的加载了url资源，即为`asset/resource` 导出 URL

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211129194953211.png" alt="image-20211129194953211" style="zoom: 50%;" />



### 2. 自定义输出文件名

2.1 默认情况下，`asset/resource` 模块以 `[hash][ext][query]` 文件名发送到输出目录。

可以通过在 webpack 配置中设置 [`output.assetModuleFilename`](https://webpack.docschina.org/configuration/output/#outputassetmodulefilename) 来修改此模板字符串：

**webpack.config.js**

```js
const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
+   assetModuleFilename: 'images/[hash][ext][query]'
  },
  module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource'
      }
    ]
  },
};
```

执行`webpack`可以看到`dist/iamges`

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211129195127680.png" alt="image-20211129195127680" style="zoom: 50%;" />

2.2 另一种自定义输出文件名的方式是，将某些资源发送到指定目录：

```js
onst path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
+   assetModuleFilename: 'images/[hash][ext][query]'
  },
  module: {
    rules: [
			{
        test: /\.png$/,
        type: 'asset/resource',
+       generator: {
+         filename: 'images/[hash][ext][query]'
+       }
      }
    ]
  },
};
```

使用此配置，所有 `png` 文件都将被发送到输出目录中的 `images` 目录中。

`Rule.generator.filename` 与 [`output.assetModuleFilename`](https://webpack.docschina.org/configuration/output/#outputassetmodulefilename) 相同，并且仅适用于 `asset` 和 `asset/resource` 模块类型。

但`Rule.generator.filename` 优先级高于 [`output.assetModuleFilename`](https://webpack.docschina.org/configuration/output/#outputassetmodulefilename) 



### 3. asset/inline

`webpack.config.js`

```js
module.exports = {
  // ...
  module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource',
        generator: {
          filename: 'images/[hash][ext][query]'
        }
      },
+     {
+       test: /\.svg$/,
+       type: 'asset/inline'
+     }
    ]
  }
}
```

`idnex.js`

```js
import empty from './assets/empty.svg'
const emptyImg = document.createElement('img')
emptyImg.style.cssText = 'width: 200px;'
emptyImg.src = empty
document.body.appendChild(emptyImg)
```

执行`webpack`，可以看到`dist`中博那个没有新增加任何资源，

执行`npx webpack-dev-server`，打开`idnex.html`可以看到图片显示出来了，并且图片`base:64` 。

 `asset/inline`只导出一个资源的 data URI，不会导出文件

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211129201922510.png" alt="image-20211129201922510" style="zoom: 50%;" />





### 4. asset/source

`webpack.config.js`

```js
module.exports = {
  // ...
  module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource',
        generator: {
          filename: 'images/[hash][ext][query]'
        }
      },
      {
        test: /\.svg$/,
        type: 'asset/inline'
      },
+     {
+       test: /\.txt$/,
+       type: 'asset/source'
+     }
    ]
  }
}
```

`assets`下新建 `hello-webapck.txt`

`src/index.js`

```js
import helloWebpack from './assets/hello-webapck.txt'
const text = document.createElement('p')
text.style.cssText = 'width: 200px;height: 200px;background-color: pink;'
text.innerText = helloWebpack
document.body.appendChild(text)
```

执行`webpack`，在`dist/images`中没有新增资源

执行`npx webpack-dev-server`，打开`index.html`可以看到`hello webpack`显示在页面上了

![image-20211129203433565](/Users/HuaJi/Library/Application Support/typora-user-images/image-20211129203433565.png)

`asset/source` 导出资源的源代码



### 5. asset 通用资源类型

`webpack.config.js`

```js
module.exports = {
  // ...
  module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource',
        generator: {
          filename: 'images/[hash][ext][query]'
        }
      },
      {
        test: /\.svg$/,
        type: 'asset/inline'
      },
      {
        test: /\.txt$/,
        type: 'asset/source'
      },
+     {
+       test: /\.jpeg$/,
+       type: 'asset'
+     }
    ]
  }
}
```

`src/index.js`

```js
import figure from './assets/figure.jpeg'
const figureImg = document.createElement('img')
figureImg.style.cssText = 'width: 200px;'
figureImg.src = figure
document.body.appendChild(figureImg)
```

执行`webpack`，在`dist/images`可以看到有新的图片，打开可以看到是我们刚刚加载的图片

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211129203931889.png" alt="image-20211129203931889" style="zoom:50%;" />

执行`npx webpack-dev-server`，打开`index.html`可以看到新图片显示在页面上了

![image-20211129204128512](/Users/HuaJi/Library/Application Support/typora-user-images/image-20211129204128512.png)

现在，`webpack` 将按照默认条件，自动地在 `resource` 和 `inline` 之间进行选择：小于 8kb 的文件，将会视为 `inline` 模块类型，否则会被视为 `resource` 模块类型。

可以通过在 `webpack` 配置的 `module rule` 层级中，设置 [`Rule.parser.dataUrlCondition.maxSize`](https://webpack.docschina.org/configuration/module/#ruleparserdataurlcondition) 选项来修改此条件：

`webpack.config.js`

```js
module.exports = {
  // ...
  module: {
    rules: [
      {
        test: /\.jpeg$/,
        type: 'asset',
+       parser: {
+         dataUrlCondition: {
+           maxSize: 4 * 1024 // 4kb
+         }
        }
      }
    ]
  }
}
```















