# 管理资源

## 1. 处理资源 - loader

webpack 只能理解 JavaScript 和 JSON 文件，这是 webpack 开箱可用的自带能力。**loader** 让 webpack 能够去处理其他类型的文件，并将它们转换为有效 [模块](https://webpack.docschina.org/concepts/modules)，以供应用程序使用，以及被添加到依赖图中。

在更高层面，在 webpack 的配置中，**loader** 有两个属性：

1. `test` 属性，识别出哪些文件会被转换。
2. `use` 属性，定义出在进行转换时，应该使用哪个 loader。

**webpack.config.js**

```javascript
const path = require('path');

module.exports = {
  output: {
    filename: 'my-first-webpack.bundle.js',
  },
  module: {
    rules: [{ test: /\.txt$/, use: 'raw-loader' }],
  },
};
```

以上配置中，对一个单独的 module 对象定义了 `rules` 属性，里面包含两个必须属性：`test` 和 `use`。这告诉 webpack 编译器(compiler) 如下信息：

> “嘿，webpack 编译器，当你碰到「在 `require()`/`import` 语句中被解析为 '.txt' 的路径」时，在你对它打包之前，先 **use(使用)** `raw-loader` 转换一下。”

<br />

### 1.1 加载css - css-loader

`css-loader` 会对 `@import` 和 `url()` 进行处理，就像 js 解析 `import/require()` 一样。

```bash
npm install --save-dev css-loader style-loader
```

`webpack.config.js`

```js
module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
};
```

**注意**这里的`style-loader`、`css-loader`顺序不要写错，loader是从右向左加载的，并且为链式调用。后执行的loader会以前面loader生成的结果进行处理资源

在`assets`目录中新建`css/style-1.css`

在`src/index.js`中引用`assets/css/style-1.css`

```js
import './assets/css/style-1.css'

const text = document.createElement('p')
text.style.cssText = 'width: 200px;height: 200px;background-color: pink;'
text.innerText = helloWebpack
text.classList.add('hello')
document.body.appendChild(text)
```

执行`npx webpack-dev-server`可以看到样式已经生效

<br />

### 1.2 处理less - less-loader

```bash
npm install less less-loader --save-dev
```

`webpack.config.js`

```js
module.exports = {
  module: {
    rules: [
      {
        test: /\.(css|less)$/i,
        use: [
          // compiles Less to CSS
          'style-loader',
          'css-loader',
          'less-loader',
        ],
      },
    ],
  },
};
```

在`assets/css`新建`style-2.less`

```css
@color: #a9a9e7;

body {
  background-color: @color;
}
```

在`src/index.js`引用`style.less`

```js
import './assets/css/style-2.less'
```

执行`npx webpcak-dev-sercer`，打开index.html 可以看到body的样式已经变化了

<br />

## 2. 加载css

### 2.1 抽离css

[MiniCssExtractPlugin](https://webpack.docschina.org/plugins/mini-css-extract-plugin/)

仅仅使用`style-loader`css只会内嵌在打包出的js文件中，最终以`style`标签作用在页面中。但这样的缺点是会使js文件变大、无法按需加载。

本插件会将 CSS 提取到单独的文件中，为每个包含 CSS 的 JS 文件创建一个 CSS 文件，并且支持 CSS 和 SourceMaps 的按需加载。

本插件基于**webpack 5**的新特性构建，并且需要 webpack 5 才能正常工作。

与 `extract-text-webpack-plugin` 相比：

- 异步加载
- 没有重复的编译（性能）
- 更容易使用
- 特别针对 CSS 开发

```bash
npm install --save-dev mini-css-extract-plugin
```

**webpack.config.js**

```js
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  // ...
  plugins: [new MiniCssExtractPlugin()],
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"], // MiniCssExtractPlugin.loader 替换 style-loader
      },
      // 或者
      {
        test: /\.(css|less)/i,
        use: [MiniCssExtractPlugin.loader, "css-loader", "less-loader"],
      },
    ],
  },
};
```

执行命令`webpack`，可以看到`dist`中生成了一个`main.css`
![在这里插入图片描述](https://img-blog.csdnimg.cn/b654f438fcd24d60aea08a869480bb21.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/dac378c1e2d1471cad5c9f44d1ea6e33.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

执行命令`npx webpack-dev-server`打开index.html可看到样式依旧加载正常

自定义分类的css文件名

```js
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  // ...
  plugins: [new MiniCssExtractPlugin({
    filename: 'styles/[hash].css'
  })],
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"], // MiniCssExtractPlugin.loader 替换 style-loader
      }
    ],
  },
};
```

执行`webpack`命令可以看到`dist/styles`中生成新的css文件

执行`npx webpcak-dev-server`打开index.html可看到页面样式正常显示

<br />

### 2.2 压缩css

[CssMinimizerWebpackPlugin](https://webpack.docschina.org/plugins/css-minimizer-webpack-plugin/)

```bash
npm install css-minimizer-webpack-plugin --save-dev
```

**webpack.config.js**

```js
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
+ const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");

module.exports = {
  module: {
    rules: [
      {
        test: /.s?css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
+ optimization: {
+   minimizer: [
+     new CssMinimizerPlugin(),
+   ],
+ },
  plugins: [new MiniCssExtractPlugin()],
};
```

这将仅在`mode: production` `生产环境`开启 CSS 优化

如果还想在`开发环境`下启用 CSS 优化，`optimization.minimize` 设置为 `true`:

**webpack.config.js**

```js
// [...]
module.exports = {
  optimization: {
    // [...]
    minimize: true,
  },
};
```

运行命令 `webpack`可以看到代码已压缩优化
![在这里插入图片描述](https://img-blog.csdnimg.cn/dfe2ade118394bcebfc4008e2a275fa0.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)
<br />

## 3. 加载images图像

前面[webpack基础篇（一）](https://blog.csdn.net/qq_41887214/article/details/121613991#t9)我们已经学习了weback内置的模块`asste modules`资源模块，成功处理了如`png|svg|jpe?g`等图片资源，那它能处理css种的图片资源吗

**webpack.config.js**

```js
 const path = require('path');

 module.exports = {
   entry: './src/index.js',
   output: {
     filename: 'bundle.js',
     path: path.resolve(__dirname, 'dist'),
   },
   module: {
     rules: [
       {
         test: /\.css$/i,
         use: ['style-loader', 'css-loader'],
       },
+      {
+        test: /\.(png|svg|jpg|jpeg|gif)$/i,
+        type: 'asset/resource',
+      },
     ],
   },
 };
```

在`src/assets/css/style-1.css`加入

```css
.text-bg {
    background-image: url("../empty.svg") !important;
}
```

在`src/index.js`中修改之前的 text 元素

```js
const text = document.createElement('p')
text.style.cssText = 'width: 200px;height: 200px;background-color: pink;'
text.innerText = helloWebpack
text.classList.add('hello')
// 添加样式
text.classList.add('text-bg')
document.body.appendChild(text)
```

执行`webpack`看到背景图片已经被打包到css中了
![在这里插入图片描述](https://img-blog.csdnimg.cn/dfb35e1b478144339a4a91f975ccfba8.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

执行`npx webpack-dev-server`可以看到背景图片已经成功加载

因为我们前面已经配置过`asset module`资源模块，所以css中的图片资源能够正常加载。这说明`asset module`也能处理css中的图片资源。

<br />

## 4. 加载 fonts 字体

那么，像字体这样的其他资源如何处理呢？使用 Asset Modules 可以接收并加载任何文件，然后将其输出到构建目录。这就是说，我们可以将它们用于任何类型的文件，也包括字体。让我们更新 `webpack.config.js` 来处理字体文件：

**webpack.config.js**

```js
 const path = require('path');

 module.exports = {
   entry: './src/index.js',
   output: {
     filename: 'bundle.js',
     path: path.resolve(__dirname, 'dist'),
   },
   module: {
     rules: [
       {
         test: /\.css$/i,
         use: ['style-loader', 'css-loader'],
       },
       {
         test: /\.(png|svg|jpg|jpeg|gif)$/i,
         type: 'asset/resource',
       },
+      {
+        test: /\.(woff|woff2|eot|ttf|otf)$/i,
+        type: 'asset/resource',
+      },
     ],
   },
 };
```

在项目中添加一些字体文件：这里我引用了iconfont在线资源

在`src/assets/css`中新建`fonts.css`

```css
/*fonts.css*/
@font-face {
  font-family: "webpack-iconfont"; /* Project id 2975967 */
  src: url('//at.alicdn.com/t/font_2975967_payhpcjfltf.woff2?t=1638248030621') format('woff2'),
       url('//at.alicdn.com/t/font_2975967_payhpcjfltf.woff?t=1638248030621') format('woff'),
       url('//at.alicdn.com/t/font_2975967_payhpcjfltf.ttf?t=1638248030621') format('truetype');
}

.webpack-iconfont {
  font-family: "webpack-iconfont" !important;
  font-size: 16px;
  font-style: normal;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

.webpack-icon-webpack:before {
  content: "\e799";
}
```

在`src/index.js`引入`fonts.css`

```js
import './assets/css/fonts.css'
```

在`index.html`中使用字体

```html
<i class="webpack-iconfont webpack-icon-webpack"></i>
```

执行`webpack`命令，可以看到
![在这里插入图片描述](https://img-blog.csdnimg.cn/361bc6b488be4c87ab7f773fa1173483.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

图片资源已经被打包进css中

执行`npx webpcak-dev-server`打开index.html可以看到，字体图标已经加载显示了
![在这里插入图片描述](https://img-blog.csdnimg.cn/dc40f0c0a4c14743a1aaa53ad326d452.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)
<br />

## 5. 加载数据

此外，可以加载的有用资源还有数据，如 JSON 文件，CSV、TSV 和 XML。类似于 NodeJS，JSON 支持实际上是内置的，也就是说 `import Data from './data.json'` 默认将正常运行。要导入 CSV、TSV 和 XML，你可以使用 [csv-loader](https://github.com/theplatapi/csv-loader) 和 [xml-loader](https://github.com/gisikw/xml-loader)。让我们处理加载这三类文件：

```bash
npm install --save-dev csv-loader xml-loader
```

**webpack.config.js**

```js
 const path = require('path');

 module.exports = {
   module: {
     rules: [
      	// ...
+      {
+        test: /\.(csv|tsv)$/i,
+        use: ['csv-loader'],
+      },
+      {
+        test: /\.xml$/i,
+        use: ['xml-loader'],
+      },
     ],
   },
 };
```

在项目中添加一些数据文件：

**src/assets/data.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<note>
  <to>Mary</to>
  <from>John</from>
  <heading>Reminder</heading>
  <body>Call Cindy on Tuesday</body>
</note>
```

**src/assets/data.csv**

```csv
to,from,heading,body
Mary,John,Reminder,Call Cindy on Tuesday
Zoe,Bill,Reminder,Buy orange juice
Autumn,Lindsey,Letter,I miss you
```

现在，你可以 `import` 这四种类型的数据(JSON, CSV, TSV, XML)中的任何一种，所导入的 `Data` 变量，将包含可直接使用的已解析 JSON：

**src/index.js**

```js
import dataXml from './assets/data.xml'
import dataCsv from './assets/data.csv'

console.log('dataXml', dataXml);
console.log('dataCsv', dataCsv);
```

执行`npm webpack-dev-server`命令，打开index.html打开工作台可以看到，两个数据已经被成功加载
![在这里插入图片描述](https://img-blog.csdnimg.cn/cc18bc3071f8488f91139c643da5eb9d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center =600x250)
<br />

### 5.1 自定义 JSON 模块 parser

通过使用 [自定义 parser](https://webpack.docschina.org/configuration/module/#ruleparserparse) 替代特定的 webpack loader，可以将任何 `toml`、`yaml` 或 `json5` 文件作为 JSON 模块导入。

假设你在 `src/assets` 文件夹下有一个 `data.toml`、一个 `data.yaml` 以及一个 `data.json5` 文件：

**src/assets/data.toml**

```toml
title = "TOML Example"

[owner]
name = "Tom Preston-Werner"
organization = "GitHub"
bio = "GitHub Cofounder & CEO\nLikes tater tots and beer."
dob = 1979-05-27T07:32:00Z
```

**src/assets/data.yaml**

```yaml
title: YAML Example
owner:
  name: Tom Preston-Werner
  organization: GitHub
  bio: |-
    GitHub Cofounder & CEO
    Likes tater tots and beer.
  dob: 1979-05-27T07:32:00.000Z
```

**src/assets/data.json5**

```json5
{
  // comment
  title: 'JSON5 Example',
  owner: {
    name: 'Tom Preston-Werner',
    organization: 'GitHub',
    bio: 'GitHub Cofounder & CEO\n\
Likes tater tots and beer.',
    dob: '1979-05-27T07:32:00.000Z',
  },
}
```

首先安装 `toml`，`yamljs` 和 `json5` 的 packages：

```bash
npm install toml yamljs json5 --save-dev
```

**webpack.config.js**

```js
const path = require('path');
+const toml = require('toml');
+const yaml = require('yamljs');
+const json5 = require('json5');

 module.exports = {
   entry: './src/index.js',
   output: {
     filename: 'bundle.js',
     path: path.resolve(__dirname, 'dist'),
   },
   module: {
     rules: [
       {
         test: /\.css$/i,
         use: ['style-loader', 'css-loader'],
       },
       {
         test: /\.(png|svg|jpg|jpeg|gif)$/i,
         type: 'asset/resource',
       },
       {
         test: /\.(woff|woff2|eot|ttf|otf)$/i,
         type: 'asset/resource',
       },
       {
         test: /\.(csv|tsv)$/i,
         use: ['csv-loader'],
       },
       {
         test: /\.xml$/i,
         use: ['xml-loader'],
       },
+      {
+        test: /\.toml$/i,
+        type: 'json',
+        parser: {
+          parse: toml.parse,
+        },
+      },
+      {
+        test: /\.yaml$/i,
+        type: 'json',
+        parser: {
+          parse: yaml.parse,
+        },
+      },
+      {
+        test: /\.json5$/i,
+        type: 'json',
+        parser: {
+          parse: json5.parse,
+        },
+      },
     ],
   },
 };
```

**src/index.js**

```js
import toml from './assets/data.toml';
import yaml from './assets/data.yaml';
import json5 from './assets/data.json5';

console.log('toml', toml);
console.log('yaml', yaml);
console.log('json5', json5);
```

执行`npx webpack-dev-server`打开index.html打开控制台可以看到数据已经加载成功
![在这里插入图片描述](https://img-blog.csdnimg.cn/8382b313a30e49b1b20a0e02699cbf27.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

<br />
<br />

源码地址：[https://gitee.com/yanhuakang/webpack-test](https://gitee.com/yanhuakang/webpack-test/tree/Webpack_Basics_3)

**`如果有用，就点个赞吧(*^▽^*)`**