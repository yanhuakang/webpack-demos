@[TOC](目录)

# 一. 资源模块

目前为止，我们的项目可以在控制台上显示"Hello world"。现在我们尝试混合一些其他资源，比如`images`，看看webpack如何处理。

在webpack出现之前，前端开发人员会使用`grunt`和`gulp`等工具来处理资源，并将它们从`/src`文件夹移动到`/dist`或`/build`目录中。

`webpack`最出色的功能之一就是，除了引入`JavaScript`，还可以内置的资源模块`asset module`引入任何其他类型的文件。

资源模块(asset module)是一种模块类型，它允许我们应用`Webpack`来打包其他资源文件（字体，图标等）而无需配置额外 loader。

在 **webpack 5** ***之前***，通常使用：

- [`raw-loader`](https://v4.webpack.js.org/loaders/raw-loader/) 将文件导入为字符串
- [`url-loader`](https://v4.webpack.js.org/loaders/url-loader/) 将文件作为 data URI 内联到 bundle 中
- [`file-loader`](https://v4.webpack.js.org/loaders/file-loader/) 将文件发送到输出目录

资源模块类型(asset module type)，通过添加 4 种新的模块类型，来**替换**所有这些 loader：

- `asset/resource` 发送一个单独的文件并导出 URL。之前通过使用 `file-loader` 实现。
- `asset/inline` 导出一个资源的 data URI。之前通过使用 `url-loader` 实现。
- `asset/source` 导出资源的源代码。之前通过使用 `raw-loader` 实现。
- `asset` 在导出一个 data URI 和发送一个单独的文件之间自动选择。之前通过使用 `url-loader`，并且配置资源体积限制实现。
<br />
## 1. asset/resource

修改 `webpack.config.js` 配置：

```js
module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource'
      }
    ]
  }
```

`index.js` 引入图片资源

```js
// src/index.js
import icon from './assets/icon.png'
const iconImg = document.createElement('img')
iconImg.style.cssText = 'width: 200px;'
iconImg.src = icon
document.body.appendChild(iconImg)
```

执行`webpack`可以在`dist`中可以看到有一个`.png`文件出现，即为`asset/resource` 发送一个单独的文件
![在这里插入图片描述](https://img-blog.csdnimg.cn/3504a1f393de43218bdd85602a9d0051.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_left =300x100)

执行`npx webpack-dev-server`打开`index.html`可以看到图片已经出现，打开控制台查看elements可以看到改img标签的加载了url资源，即为`asset/resource` 导出 URL
![在这里插入图片描述](https://img-blog.csdnimg.cn/48f1f358060844d181b5a5fd89458efe.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_left =500x400)

## 2. 自定义输出文件名

2.1 默认情况下，`asset/resource` 模块以 `[hash][ext][query]` 文件名发送到输出目录。

可以通过在 webpack 配置中设置 [`output.assetModuleFilename`](https://webpack.docschina.org/configuration/output/#outputassetmodulefilename) 来修改此模板字符串：

**webpack.config.js**

```js
const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
+   assetModuleFilename: 'images/[hash][ext][query]'
  },
  module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource'
      }
    ]
  },
};
```

执行`webpack`可以看到`dist/images`
![在这里插入图片描述](https://img-blog.csdnimg.cn/84916c921ffb439193494126922f1c09.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_left =300x100)


2.2 另一种自定义输出文件名的方式是，将某些资源发送到指定目录：

```js
onst path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
+   assetModuleFilename: 'images/[hash][ext][query]'
  },
  module: {
    rules: [
			{
        test: /\.png$/,
        type: 'asset/resource',
+       generator: {
+         filename: 'images/[hash][ext][query]'
+       }
      }
    ]
  },
};
```

使用此配置，所有 `png` 文件都将被发送到输出目录中的 `images` 目录中。

`Rule.generator.filename` 与 [`output.assetModuleFilename`](https://webpack.docschina.org/configuration/output/#outputassetmodulefilename) 相同，并且仅适用于 `asset` 和 `asset/resource` 模块类型。

但`Rule.generator.filename` 优先级高于 [`output.assetModuleFilename`](https://webpack.docschina.org/configuration/output/#outputassetmodulefilename) 


<br />

## 3. asset/inline

`webpack.config.js`

```js
module.exports = {
  // ...
  module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource',
        generator: {
          filename: 'images/[hash][ext][query]'
        }
      },
+     {
+       test: /\.svg$/,
+       type: 'asset/inline'
+     }
    ]
  }
}
```

`idnex.js`

```js
import empty from './assets/empty.svg'
const emptyImg = document.createElement('img')
emptyImg.style.cssText = 'width: 200px;'
emptyImg.src = empty
document.body.appendChild(emptyImg)
```

执行`webpack`，可以看到`dist`中并没有新增加任何资源，
![在这里插入图片描述](https://img-blog.csdnimg.cn/6019229140f84f8fbeb8340eb7da987c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_left =300x100)

执行`npx webpack-dev-server`，打开`idnex.html`可以看到图片显示出来了，并且图片`base:64` 。
![在这里插入图片描述](https://img-blog.csdnimg.cn/2f304deb71b34814b3b46464c2081a22.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_left =600x400)

 `asset/inline`只导出一个资源的 data URI，不会导出文件


<br />

## 4. asset/source

`webpack.config.js`

```js
module.exports = {
  // ...
  module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource',
        generator: {
          filename: 'images/[hash][ext][query]'
        }
      },
      {
        test: /\.svg$/,
        type: 'asset/inline'
      },
+     {
+       test: /\.txt$/,
+       type: 'asset/source'
+     }
    ]
  }
}
```

`assets`下新建 `hello-webapck.txt`

`src/index.js`

```js
import helloWebpack from './assets/hello-webapck.txt'
const text = document.createElement('p')
text.style.cssText = 'width: 200px;height: 200px;background-color: pink;'
text.innerText = helloWebpack
document.body.appendChild(text)
```

执行`webpack`，在`dist/images`中没有新增资源

执行`npx webpack-dev-server`，打开`index.html`可以看到`hello webpack`显示在页面上了
![在这里插入图片描述](https://img-blog.csdnimg.cn/7e51eed355e34ce08fd639bd7c10d1d4.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_left)

`asset/source` 导出资源的源代码


<br />

## 5. asset 通用资源类型

`webpack.config.js`

```js
module.exports = {
  // ...
  module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource',
        generator: {
          filename: 'images/[hash][ext][query]'
        }
      },
      {
        test: /\.svg$/,
        type: 'asset/inline'
      },
      {
        test: /\.txt$/,
        type: 'asset/source'
      },
+     {
+       test: /\.jpeg$/,
+       type: 'asset'
+     }
    ]
  }
}
```

`src/index.js`

```js
import figure from './assets/figure.jpeg'
const figureImg = document.createElement('img')
figureImg.style.cssText = 'width: 200px;'
figureImg.src = figure
document.body.appendChild(figureImg)
```

执行`webpack`，在`dist/images`可以看到有新的图片，打开可以看到是我们刚刚加载的图片
![在这里插入图片描述](https://img-blog.csdnimg.cn/25ac5d6cc0a147c886ad0b6df1761047.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_left =300x100)

执行`npx webpack-dev-server`，打开`index.html`可以看到新图片显示在页面上了
![在这里插入图片描述](https://img-blog.csdnimg.cn/9e0f1a16812d4ab3ac0c74c04098a527.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_left)

现在，`webpack` 将按照默认条件，自动地在 `resource` 和 `inline` 之间进行选择：小于 8kb 的文件，将会视为 `inline` 模块类型，否则会被视为 `resource` 模块类型。

可以通过在 `webpack` 配置的 `module rule` 层级中，设置 [`Rule.parser.dataUrlCondition.maxSize`](https://webpack.docschina.org/configuration/module/#ruleparserdataurlcondition) 选项来修改此条件：

`webpack.config.js`

```js
module.exports = {
  // ...
  module: {
    rules: [
      {
        test: /\.jpeg$/,
        type: 'asset',
+       parser: {
+         dataUrlCondition: {
+           maxSize: 4 * 1024 // 4kb
+         }
        }
      }
    ]
  }
}
```




<br />
<br />


源码地址：[https://gitee.com/yanhuakang/webpack-test](https://gitee.com/yanhuakang/webpack-test/tree/Webpack_Basics_2)


**`如果有用，就点个赞吧(*^▽^*)`**