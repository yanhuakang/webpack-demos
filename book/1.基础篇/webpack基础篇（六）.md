# 缓存

[caching](https://webpack.docschina.org/guides/caching/)

## 前言：

我们使用 `webpack` 来打包我们的模块化后的应用程序，`webpack` 会生成一个可部署的` /dist`目录，然后把打包后的内容放置在此目录中。只要` /dist` 目录中的内容部署到 `server` 上，`client`（通常是浏览器）就能够访问此 `server` 的网站及其资源。而最后一步获取资源是比较耗费时间的，这就是为什么浏览器使用一种名为 `缓存` 的技术。

可以通过命中缓存，以降低网络流量，使网站加载速度更快，然而，如果我们在部署新版本时不更改资源的文件名，浏览器可能会认为它没有被更新，就会使用它的缓存版本。由于缓存的存在，当你需要获取新的代码时，就会显得很棘手。

通过必要的配置，以确保 webpack 打包生成的文件能够被客户端缓存，而在文件内容变化后，能够请求到新的文件。



## 1. 输出文件的文件名(output filename)

我们可以通过替换 `output.filename` 中的 [substitutions](https://webpack.docschina.org/configuration/output/#outputfilename) 设置，来定义输出文件的名称。webpack 提供了一种使用称为 **substitution(可替换模板字符串)** 的方式，通过带括号字符串来模板化文件名。其中，`[contenthash]` substitution 将根据资源内容创建出唯一 hash。当资源内容发生变化时，`[contenthash]` 也会发生变化。

**webpack.config.js**

```js
const path = require('path');

module.exports = {
  entry: {
    index: './src/index.js',
    another: './src/another_module.js',
  },
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, 'dist'),
  },
};
```

执行`webpack`可以看到打包出的js文件已经带有 `contenthash` 

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211201125301007.png" alt="image-20211201125301007" style="zoom:50%;" />

执行`npx webpack-dev-server`可以看到页面加载正常，控制台打印结果也是正常的



<br />



## 2. 缓存第三方库

将第三方库(`library`)（例如 `lodash` ）提取到单独的 `vendor chunk` 文件中，是比较推荐的做法，这是因为，它们很少像本地的源代码那样频繁修改。因此通过实现以上步骤，利用 `client` 的长效缓存机制，命中缓存来消除请求，并减少向 `server` 获取资源，同时还能保证 `client` 代码和 `server` 代码版本一致。 我们在 `optimization.splitChunks` 添加如下 `cacheGroups` 参数并构建：

**webpack.config.js**

```js
module.exports = {
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          // 第三方模块一般处于node_modules中，所以这里缓存 node_modules 中第三方的模块
          test: /[\\/]node_modules[\\/]/, // 缓存node_modules中的文件夹名和文件（文件目录前后可能会有斜线/）
          name: 'vendors',
          chunks: 'all'
        }
      }
    }
  },
};
```

执行`webpack`可以看到打包结果

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211201130813293.png" alt="image-20211201130813293" style="zoom:50%;" />

执行`npx webpcak-dev-server`可以看到页面加载正常



<br />



## 3. **将**  **js** 文件放到一个文件夹中

目前，全部 js 文件都在 dist 文件夹根目录下，我们尝试把它们放到一个文件夹中，这个其实也简单，修改配置文件：

**webpack.config.js**

```js
module.exports = {
  // ...
  output: {
    // filename 在前面加上路径即可
    filename: 'js/[name].[contenthash].js',
  },
};
```

执行`webpack`可以看到，js文件都打包到 `js` 目录中了

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211201131224460.png" alt="image-20211201131224460" style="zoom:50%;" />

结合之前的文章[webpack基础篇（二）：资源模块asset module](https://blog.csdn.net/qq_41887214/article/details/121631683)、[webpack基础篇（三）：管理资源](https://blog.csdn.net/qq_41887214/article/details/121636739)，目前为止，我们已经把 JS 文件、css文件及images、fonts等资源文件分别放到了 `js` 、 `styles` 、 `images` 三个文件夹中。



<br />

<br />



源码地址：[https://gitee.com/yanhuakang/webpack-test](https://gitee.com/yanhuakang/webpack-test/tree/Webpack_Basics_6)

**`如果有用，就点个赞吧(\*^▽^\*)`**