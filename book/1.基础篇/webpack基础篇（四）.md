# 一. babel-loader

## 1.1 为什么使用babel-loader

webpack本身只能加载、打包js模块，并不能将一些代码转化为浏览器识别的代码。

如今 ES6 语法在开发中已经非常普及，甚至也有许多开发人员用上了 ES7 或 ES8 语法。然而，浏览器对这些高级语法的支持性并不是非

常好。因此为了让我们的新语法能在浏览器中都能顺利运行，Babel 应运而生。

`Babel`是一个JavaScript编译器，能够让我们放心的使用新一代JS语法

<br />

## 1.2 使用babel-loader

```bash
npm install -D babel-loader @babel/core @babel/preset-env
```

- `babel-loader`：**在webpack里应用 babel 解析ES6的桥梁**

- `@babel/core`：**babel核心模块**

- `@babel/preset-env`：**babel预设，内置一组 babel 插件的集合**

在 webpack 配置对象中，需要将 `babel-loader` 添加到 `module` 列表中，就像下面这样

**webpack.config.js**

```js
module: {
  rules: [
    {
      test: /\.js$/,
      exclude: /node_modules/, // 排除编译 node_modules
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    }
  ]
}
```

**src/test.js**

```js
export function getData () {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Hello World!')
    }, 2000)
  })
}
```

**src/index.js**

```js
import { getData } from './test.js'

const getList = async () => {
 const data = await getData()
  console.log('data:', data);
}
getList()
```

执行`npx webpack-dev-server`命令，打开index.html打开控制台查看打印结果
![在这里插入图片描述](https://img-blog.csdnimg.cn/e183af098b844a91b992fe0f277d4d8b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

报错了

<br />

## 1.3 **regeneratorRuntime** **插件**

`regeneratorRuntime` 是webpack打包生成的全局辅助函数，由babel生成，用于兼容`async/await`的语法。

`regeneratorRuntime is not defined` 这个错误显然是未能正确配置`babel`。

正确的做法需要添加以下的插件和配置：

```bash
# 这个包中包含了regeneratorRuntime，运行时需要
npm install --save @babel/runtime

# 这个插件会在需要regeneratorRuntime的地方自动require导包，编译时需要 
npm install --save-dev @babel/plugin-transform-runtime

# 更多参考这里 
https://babeljs.io/docs/en/babel-plugin-transform-runtime
```

**webpack.config.js**

```js
module: {
  rules: [
    {
      test: /\.js$/,
      exclude: /node_modules/, // 排除编译 node_modules
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'],
          plugins: ['@babel/plugin-transform-runtime'],
        }
      }
    }
  ]
}
```

执行`npx webpack-dev-server`命令，打开index.html打开控制台查看打印结果，两秒后打印了"data: Hello World!"

<br />
<br />

源码地址：[https://gitee.com/yanhuakang/webpack-test](https://gitee.com/yanhuakang/webpack-test/tree/Webpack_Basics_3)

**`如果有用，就点个赞吧(*^▽^*)`**