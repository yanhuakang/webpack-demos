您可以将配置选项的散列传递给`html-webpack-plugin`. 允许的值如下：

| 名称                     | 类型                            | 默认                                          | 描述                                                         |
| ------------------------ | ------------------------------- | --------------------------------------------- | ------------------------------------------------------------ |
| **`title`**              | `{String}`                      | `Webpack App`                                 | 用于生成的 HTML 文档的标题                                   |
| **`filename`**           | `{String|Function}`             | `'index.html'`                                | 要将 HTML 写入到的文件。默认为`index.html`. 您也可以在此处指定子目录（例如：）`assets/admin.html`。该`[name]`占位符将与输入的名称所取代。也可以是一个函数，例如`(entryName) => entryName + '.html'`。 |
| **`template`**           | `{String}`                      | ``                                            | `webpack`模板的相对或绝对路径。默认情况下，`src/index.ejs`如果存在，它将使用。请参阅[文档](https://github.com/jantimon/html-webpack-plugin/blob/master/docs/template-option.md)了解详细信息 |
| **`templateContent`**    | `{string|Function|false}`       | 错误的                                        | 可用于代替`template`提供内联模板 - 请阅读[编写您自己的模板](https://github.com/jantimon/html-webpack-plugin#writing-your-own-templates)部分 |
| **`templateParameters`** | `{Boolean|Object|Function}`     | `false`                                       | 允许覆盖模板中使用的参数 - 请参阅[示例](https://github.com/jantimon/html-webpack-plugin/tree/master/examples/template-parameters) |
| **`inject`**             | `{Boolean|String}`              | `true`                                        | `true || 'head' || 'body' || false`将所有资产注入给定的`template`or `templateContent`。传递时，`'body'`所有 javascript 资源都将放置在 body 元素的底部。`'head'`将脚本放在 head 元素中。传递`true`将根据`scriptLoading`选项将其添加到头部/身体。通过`false`将禁用自动注射。- 请参阅[注入：假示例](https://github.com/jantimon/html-webpack-plugin/tree/master/examples/custom-insertion-position) |
| **`publicPath`**         | `{String|'auto'}`               | `'auto'`                                      | 用于脚本和链接标签的 publicPath                              |
| **`scriptLoading`**      | `{'blocking'|'defer'|'module'}` | `'defer'`                                     | 现代浏览器支持非阻塞 javascript 加载 ( `'defer'`) 以提高页面启动性能。设置为`'module'`添加属性[`type="module"`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules#applying_the_module_to_your_html)。这也意味着“延迟”，因为模块会自动延迟。 |
| **`favicon`**            | `{String}`                      | ``                                            | 将给定的图标路径添加到输出 HTML                              |
| **`meta`**               | `{Object}`                      | `{}`                                          | 允许注入`meta`-tags。例如`meta: {viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'}` |
| **`base`**               | `{Object|String|false}`         | `false`                                       | 注入[`base`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base)标签。例如`base: "https://example.com/path/page.html` |
| **`minify`**             | `{Boolean|Object}`              | `true`如果`mode`是`'production'`，否则`false` | 控制是否以及以何种方式应该缩小输出。有关更多详细信息，请参阅下面的[缩小](https://github.com/jantimon/html-webpack-plugin#minification)。 |
| **`hash`**               | `{Boolean}`                     | `false`                                       | 如果`true`然后将唯一的`webpack`编译哈希附加到所有包含的脚本和 CSS 文件。这对于缓存破坏很有用 |
| **`cache`**              | `{Boolean}`                     | `true`                                        | 仅当文件被更改时才发出文件                                   |
| **`showErrors`**         | `{Boolean}`                     | `true`                                        | 错误详细信息将写入 HTML 页面                                 |
| **`chunks`**             | `{?}`                           | `?`                                           | 允许您仅添加一些块（例如，仅单元测试块）                     |
| **`chunksSortMode`**     | `{String|Function}`             | `auto`                                        | 允许控制块在被包含到 HTML 之前应该如何排序。允许的值为`'none' | 'auto' | 'manual' | {Function}` |
| **`excludeChunks`**      | `{Array.<string>}`              | ``                                            | 允许您跳过一些块（例如，不要添加单元测试块）                 |
| **`xhtml`**              | `{Boolean}`                     | `false`                                       | 如果`true`将`link`标签呈现为自闭合（XHTML 兼容）             |