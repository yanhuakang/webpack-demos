# PostCSS **与** CSS Modules

- [PostCSS](https://www.postcss.com.cn/) 是一个用 JavaScript 工具和插件转换 CSS 代码的工具。比如可以使用 [Autoprefixer](https://github.com/postcss/autoprefixer#readme) 插件自动获取浏览器的流行度和能够支

  持的属性，并根据这些数据帮我们自动的 为 CSS 规则添加前缀，将最新的 CSS 语法转换成大多数浏览器都能理解的语法。

- [CSS Modules](https://github.com/css-modules/css-modules) 能让你永远不用担心命名太大众化而造成冲突，只要用最有意义的名字就行了。

![image-20211210165722570](/Users/HuaJi/Library/Application Support/typora-user-images/image-20211210165722570.png)

## PostCSS

如果要使用最新版本的话，你需要使用 ` webpack v5`。如果使用 `webpack v4` 的话，你需要安装 `postcss-loader v4`。

为了使用本 loader，你需要安装 `postcss-loader` 和 `postcss`：

```bash
npm install -D style-loader css-loader postcss-loader postcss
```

可以使用 `autoprefixer` 插件为CSS 规则添加前缀

```bash
npm i --save-dev autoprefixer
```

可以使用 `postcss-nested` 插件为CSS提供编写嵌套的样式语法支持

```bash
npm i -D postcss-nested
```

然后添加本 loader 的相关配置到你的 `webpack` 的配置文件。例如：

```css
// css
body {
    background-color: chocolate;
    display: flex;

    /* postcss-nested 提供嵌套语法支持 */
    .hello {
        color: #a9a9e7;
    }
}
```

**webpack.config.js**

```js
module.exports = {
  module: {
    rules: [
      {
        test: /\.(css|less)/i,
        use: [
          'style-loader',
          'css-loader',
          // 'less-loader', // 也可以使用less-loader（npm i -D less-loader）
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: { // 也可使用 PostCSS 配置文件 postcss.config.js 代替
                plugins: [
                  require('autoprefixer'), // 为CSS 规则添加前缀
                  require('postcss-nested'), // 为CSS提供编写嵌套的样式语法
                ],
              },
            },
          },
        ],
      },
    ],
  },
};
```

`postcssOptions` 也可使用 PostCSS 配置文件 `postcss.config.js` 代替

**postcss.config.js**

```js
module.exports = {
  plugins: [
    require('autoprefixer'), // 为CSS 规则添加前缀
    require('postcss-nested'), // 为CSS提供编写嵌套的样式语法
  ],
};
```

**package.json**

```json
{
  // ...
  "browserslist": [ // 更多的 browserslist 请参阅https://github.com/browserslist/browserslist#readme
    "> 1%", // 支持在全球使用率超过1%的浏览器
    "last 2 versions" // 每个浏览器中最新的两个版本，注意 versions，带s
  ]
}
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211210165500132.png" alt="image-20211210165500132" style="zoom:50%;" />



<br />



## CSS Modules

```bash
npm i -D style-loader css-loader
```

**webpack.config.js**

```js
module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true, // 启用 CSS 模块
            },
          },
        ],
      },
    ],
  },
};
```

modules 更具体的配置项参考：[css-loader | webpack 中文文档](https://webpack.docschina.org/loaders/css-loader/)

loader 会用唯一的标识符 (identifier) 来替换局部选择器。所选择的唯一标识符以模块形式暴露出去。

**webpack.config.js**

```js
module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: {
                mode: 'local',
                // 样式名规则配置
                localIdentName: '[name]__[local]--[hash:base64:5]',
              },
            },
          },
        ],
      },
    ],
  },
};
```

**App.css**

```css
.header__wrapper {
  text-align: center;
}
 
.title {
  color: gray;
  font-size: 34px;
  font-weight: bold;
}
 
.sub-title {
  color: green;
  font-size: 16px;
}
```

**App.js**

```js
...
import styles from "./App.css";
...
<div>
  <header className={styles["header__wrapper"]}>
    <h1 className={styles["title"]}>标题</h1>
    <div className={styles["sub-title"]}>描述</div>
  </header>
</div>
```

编译打包后的 CSS，class 增加了 `hash` 值。

```css
.App__header__wrapper--2_jgY {
  text-align: center;
}
 
.App__title--GpMto {
  color: gray;
  font-size: 34px;
  font-weight: bold;
}
 
.App__sub-title--1cIFi {
  color: green;
  font-size: 16px;
}
```

![1608016e57194b3f99816902289c79ca](/Users/HuaJi/Downloads/1608016e57194b3f99816902289c79ca.png)



你还可以参阅：

[CSS Modules 用法教程 - 阮一峰的网络日志](https://www.ruanyifeng.com/blog/2016/06/css_modules.html)

[如何在 React 中优雅的写 CSS？](https://blog.csdn.net/qq_41887214/article/details/121579964)



<br />



**`继续加油 ヾ(◍°∇°◍)ﾉﾞ`**











