## Source Map

此选项控制是否生成，以及如何生成 source map。

使用 [`SourceMapDevToolPlugin`](https://webpack.docschina.org/plugins/source-map-dev-tool-plugin) 进行更细粒度的配置。查看 [`source-map-loader`](https://webpack.docschina.org/loaders/source-map-loader) 来处理已有的 source map。

选择一种 `source map` 格式来增强调试过程。不同的值会明显影响到构建(build)和重新构建(rebuild)的速度。

### Devtool模式

| Devtool                        | 表现                            | 生产 | 质量                 | 评论                                           |
| ------------------------------ | ------------------------------- | ---- | -------------------- | ---------------------------------------------- |
| `none`                         | **构建**：最快 **重建**：最快   | 是的 | `打包后的代码`       | 具有最高性能的生产版本的推荐选择。             |
| **`eval`**                     | **构建**：快速 **重建**：最快   | 不   | `生成后的代码`       | 具有最高性能的开发版本的推荐选择。             |
| `eval-cheap-source-map`        | **构建**：好的 **重建**：快速   | 不   | `转换过的代码`       | 开发构建的权衡选择。                           |
| `eval-cheap-module-source-map` | **构建**：缓慢 **重建**：快速   | 不   | `只映射行信息的源码` | 开发构建的权衡选择。                           |
| **`eval-source-map`**          | **构建**：最慢的 **重建**：好的 | 不   | `源码`               | 使用高质量 SourceMaps 进行开发构建的推荐选择。 |
| **`source-map`**               | **构建**：最慢 **重建**： 最慢  | 是的 | `源码`               | 使用高质量 SourceMaps 进行生产构建的推荐选择。 |
| `inline-source-map`            | **构建**：最慢 **重建**：最慢   | 不   | `源码`               | 发布单个文件时的可能选择                       |
| `hidden-source-map`            | **构建**：最慢 **重建**：最慢   | 是的 | `源码`               | 仅将 SourceMap 用于错误报告目的时的可能选择。  |

其中一些值适用于开发环境，一些适用于生产环境。

对于开发环境，通常希望更快速的 source map，需要添加到 bundle 中以增加体积为代价，

但是对于生产环境，则希望更精准的 source map，需要从 bundle 中分离并独立存在。



### 品质说明(quality)

`打包后的代码` - 将所有生成的代码视为一大块代码。你看不到相互分离的模块。

`生成后的代码` - 每个模块相互分离，并用模块名称进行注释。可以看到 webpack 生成的代码。

`转换过的代码` - 每个模块相互分离，并用模块名称进行注释。可以看到 webpack 转换前、loader 转译后的代码。

`源代码` - 每个模块相互分离，并用模块名称进行注释。你会看到转译之前的代码，正如编写它时。这取决于 loader 支持。

`（仅限行）` - source map 被简化为每行一个映射。这通常意味着每个语句只有一个映射（假设你使用这种方式）。这会妨碍你在语句级别上调试执行，也会妨碍你在每行的一些列上设置断点。与压缩后的代码组合后，映射关系是不可能实现的，因为压缩工具通常只会输出一行。



### 对于开发环境

以下选项非常适合开发环境：

- `eval-source-map` - 每个模块使用 `eval()` 执行，并且 source map 转换为 DataUrl 后添加到 `eval()` 中。初始化 source map 时比较慢，但是会在重新构建时提供比较快的速度，并且生成实际的文件。行数能够正确映射，因为会映射到原始代码中。它会生成用于开发环境的最佳品质的 source map。

- `eval-cheap-source-map` - 类似 `eval-source-map`，每个模块使用 `eval()` 执行。这是 "cheap(低开销)" 的 source map，因为它没有生成列映射(column mapping)，只是映射行数。它会忽略源自 loader 的 source map，并且仅显示转译后的代码，就像 `eval` devtool。

- `eval-cheap-module-source-map` - 类似 `eval-cheap-source-map`，并且，在这种情况下，源自 loader 的 source map 会得到更好的处理结果。然而，loader source map 会被简化为每行一个映射(mapping)。



### 对于生产环境

这些选项通常用于生产环境中：

- `(none)`（省略 `devtool` 选项） - 不生成 source map。这是一个不错的选择。

- `source-map` - 整个 source map 作为一个单独的文件生成。它为 bundle 添加了一个引用注释，以便开发工具知道在哪里可以找到它。

> **`警告`**
>
> 你应该将你的服务器配置为，不允许普通用户访问 source map 文件！

- `hidden-source-map` - 与 `source-map` 相同，但不会为 bundle 添加引用注释。如果你只想 source map 映射那些源自错误报告的错误堆栈跟踪信息，但不想为浏览器开发工具暴露你的 source map，这个选项会很有用。

> **`警告`**
>
> 你不应将 source map 文件部署到 web 服务器。而是只将其用于错误报告工具。





## devServer

开发环境下，我们往往需要启动一个web服务，方便我们模拟一个用户从浏览器中访问我们的web服务，读取我们的打包产物，以观测我

们的代码在客户端的表现。webpack5内置了这样的功能，我们只需要简单的配置就可以开启它。在此之前，我们需要安装它

```bash
npm i -D webpack-dev-server
```

### 基础使用：

**webpack.config.js**

```javascript
const path = require('path');

module.exports = {
  //...
  devServer: {
    static: { // 默认是把/dist目录当作web服务的根目录
      directory: path.join(__dirname, 'public'),
    },
    compress: true, // 可选择开启gzip压缩功能，对应静态资源请求的响应头里的，默认开启
    port: 9000, // 端口号
  },
};
```

当服务(`server`)启动后，在解析模块列表之前输出一条消息：

![image-20211204211409732](/Users/HuaJi/Library/Application Support/typora-user-images/image-20211204211409732.png)

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211204212839354.png" alt="image-20211204212839354" style="zoom:50%;" />

这里将会给出服务启动位置以及内容的一些基本信息。



### 添加响应头

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211204212545909.png" alt="image-20211204212545909" style="zoom:50%;" />

有些场景需求下，我们需要为所有响应添加headers,来对资源的请求和响应打入标志，以便做一些安全防范，或者方便发生异常后做请求

的链路追踪。比如:

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    headers: {
      'X-Custom-Foo': 'bar',
    },
  },
};
```

你也可以传递一个数组：

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    headers: [
      {
        key: 'X-Custom',
        value: 'foo',
      },
      {
        key: 'Y-Custom',
        value: 'bar',
      },
    ],
  },
};
```

你也可以传递一个函数：

```javascript
module.exports = {
  //...
  devServer: {
    headers: () => {
      return { 'X-Bar': ['key1=value1', 'key2=value2'] };
    },
  },
};
```



### hot

`'only'` `boolean = true`

启用 webpack 的 [热模块替换](https://webpack.docschina.org/concepts/hot-module-replacement/) 特性：

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    hot: true,
  },
};
```

> ###### Tip
>
> 从 webpack-dev-server v4 开始，devServer.hot 是默认启用的。它会自动应用 [`webpack.HotModuleReplacementPlugin`](https://webpack.docschina.org/plugins/hot-module-replacement-plugin/)



### open

`boolean` `string` `[string]` `object` `[object]`

告诉 dev-server 在服务器已经启动后打开浏览器。设置其为 `true` 以打开你的默认浏览器。

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    open: true,
  },
};
```



### 开启代理 proxy

当后端在 `localhost:3000` 上，客户端在`localhost:9000`，可以使用它来启用代理解决跨域：

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    proxy: {
      '/api': 'http://localhost:3000',
    },
  },
};
```

现在，对 `/api/users` 的请求会将请求代理到 `http://localhost:3000/api/users`。

如果不希望传递`/api`，则需要重写路径：

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        pathRewrite: { '^/api': '' },
      },
    },
  },
};
```



#### 有时不想代理所有内容。 可以基于函数的返回值绕过代理。

在该功能中，可以访问请求，响应和代理选项。

- 返回 `null` 或 `undefined` 以继续使用代理处理请求。
- 返回 `false` 会为请求产生 404 错误。
- 返回提供服务的路径，而不是继续代理请求。

例如。 对于浏览器请求，想要提供 HTML 页面，但是对于 API 请求，想要代理它。 可以执行以下操作：

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        bypass: function (req, res, proxyOptions) {
          if (req.headers.accept.indexOf('html') !== -1) {
            console.log('Skipping proxy for browser request.');
            return '/index.html';
          }
        },
      },
    },
  },
};
```





#### 将多个特定路径代理到同一目标

可以使用一个或多个带有 `context` 属性的对象的数组：

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    proxy: [
      {
        context: ['/auth', '/api'],
        target: 'http://localhost:3000',
      },
    ],
  },
};
```



#### changeOrigin

默认情况下，代理时会保留主机头的来源，可以将 `changeOrigin` 设置为 `true` 以覆盖此行为。 在某些情况下，例如使用 [name-based virtual hosted sites](https://en.wikipedia.org/wiki/Virtual_hosting#Name-based)，它很有用。

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        changeOrigin: true,
      },
    },
  },
};
```

如果设置成true：发送请求头中host会设置成target





### client

#### overlay

`boolean = true` `object: { errors boolean = true, warnings boolean = true }`

当出现编译错误或警告时，在浏览器中显示全屏覆盖。

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    client: {
      overlay: true,
    },
  },
};
```



如果你只想显示错误信息：

**webpack.config.js**

```javascript
module.exports = {
  //...
  devServer: {
    client: {
      overlay: {
        errors: true,
        warnings: false,
      },
    },
  },
};
```



### progress

```
boolean
```

在浏览器中以百分比显示编译进度。

**webpack.config.js**

```js
module.exports = {
  //...
  devServer: {
    client: {
      progress: true,
    },
  },
};
```