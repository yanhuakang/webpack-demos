# Git Hooks 与 Husky

在项目规范化开发过程中，我们希望同事提交的代码都是符合代码规范的。我们可以使用 `ESLint` 格式化工具来校验代码；

但是如何来强有力的保证大家提交到远程仓库的代码一定是经过 `ESLint` 检查并修复的呢？

`Git Hooks` 来解决这个问题



## 一、Git Hooks

### 1.1 Git Hooks是什么？

Git Hooks 在Git执行特定事件（如commit、push、receive等）后触发运行脚本

让我们直观的看一下它

1. 创建一个 `.git` 本地仓库

```bash
git init
```



2. 查看 `.git`

```shell
ls -la
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207204752936.png" alt="image-20211207204752936" style="zoom:50%;" />



3. 查看 `hooks`

```shell
cd hooks
ls -la
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207205058142.png" alt="image-20211207205058142" style="zoom:50%;" />

这是一个还没有配置 `Git Hooks` 的仓库，默认会有很多`.sample`结尾的文件，是示例文件，不会生效。

这些 `hooks` 会在我们执行 `git` 时或提前、或滞后自动执行

**完整钩子说明，请参考**[官网链接](https://link.zhihu.com/?target=https%3A//git-scm.com/docs/githooks)



4. 查看 `.sample` 文件

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207210321647.png" alt="image-20211207210321647" style="zoom:50%;" />

该文件告诉我们，去除 `.sample` 重命名该文件让该hook生效



### 1.2 配置 pre-commit

1.2.1 配置 `pre-commit` 实现提交代码时强制 `ESLint` 校验

```shell
touch pre-commit
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207205902488.png" alt="image-20211207205902488" style="zoom:50%;" />

我们新增了一个文件 `pre-commit` 用来让 `pre-commit` hook 生效



1.2.2 编辑 `pre-commit`

```shell
vim pre-commit
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207211333958.png" alt="image-20211207211333958" style="zoom:50%;" />

按下键盘 `i` 键（insert）输入 `npx eslint ./src/`，即校验 `src` 下的文件

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207211523535.png" alt="image-20211207211523535" style="zoom:50%;" />

按下键盘 `esc` 退出输出

输出 `:wq` 退出编辑

给 `pre-commit` 添加可执行权限

```shell
chmod +x ./pre-commit
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207212259249.png" alt="image-20211207212259249" style="zoom:50%;" />

回到项目根目录

```shell
cd ../../
```



`git commit` 提交代码（使用 `IDE` 提交代码也可以）

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207212520657.png" alt="image-20211207212520657" style="zoom:50%;" />

可以看到 `ESLint` 校验生效了，如果有未修复的问题则会让 `git commit` 失败



注：如果你出现以下问题，说明 `pre-commit` 没有可执行权限，安装上面添加一下就好了

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207212008891.png" alt="image-20211207212008891" style="zoom: 67%;" />



### 1.3 Git Hooks 问题

- 配置较麻烦（实际上也不麻烦 ^_^）

- `.git` 无法上传到远程仓库，配置无法共享

我们总不能手动copy配置吧，这样实在不高级，不优雅 😁



## 二、手动实现 Husky 

在项目根目录下创建 `.mygithooks`目录，在 `.mygithooks` 下新建 `pre-commit` 

```
mkdir .mygithooks
cd .mygithooks/
touch pre-commit
```

修改**pre-commit**

```bash
npx eslint ./src/
```



配置 `git` 让 `.mygithooks/pre-commit` 被 git 运行

```bash
git config core.hooksPath .mygithooks
```

给 `pre-commit` 添加可执行权限

```shell
chmod +x ./pre-commit
```



查看是否配置成功

```shell
cd ../.git
cat config
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207214941996.png" alt="image-20211207214941996" style="zoom:50%;" />

可以看到已经配置成功



回到根目录

```bash
git add .
git commit -m 'msg' //（使用 `IDE` 提交代码也可以）
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207220541198.png" alt="image-20211207220541198" style="zoom:50%;" />

可以看到 `ESLint` 校验成功了

这样将 `.mygithooks` 上传至远程仓库，你的小伙伴也可以共享 `git hooks` 配置了



## 三、Husky

在使用 `Husky` 之前，需要移除我们刚刚配置的 `.mygithooks`

```shell
cd .git
vim config
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207221448857.png" alt="image-20211207221448857" style="zoom:50%;" />

 光标选中 **`hooksPath`**，注意不要选错

双击按键 `D` 删除

按键 `esc` 结束编辑

按键 `:wq` 退出

回到项目根目录

```shell
cd ../
```





### 3.1 配置 husky

[husky 官网](https://typicode.github.io/husky/#/?id=manual)

3.1.1 安装`husky`

```shell
npm install husky --save-dev
```



3.1.2 启用 Git hooks

```shell
npx husky install
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207222617308.png" alt="image-20211207222617308" style="zoom:50%;" />

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207222631058.png" alt="image-20211207222631058" style="zoom:50%;" />

可以看到在在我们项目的根目录出现了一个 `.husky` 文件夹，实质上与我们创建 `.mygithooks` 是一样的



3.1.3 创建 `pre-commit` hook

```shell
npx husky add .husky/pre-commit "npm test"
git add .husky/pre-commit
```

或者手动创建也可以

编辑 ` pre-commit`

```shell
npx eslint ./src/
```

给 `pre-commit` 添加可执行权限

```shell
chmod +x .husky/pre-commit 
```



3.1.4 提交代码，测试一下

```bash
git commit -m 'husky'
```

<img src="/Users/HuaJi/Library/Application Support/typora-user-images/image-20211207224041978.png" alt="image-20211207224041978" style="zoom:50%;" />

可以看到 `ESLint` 检查成功了

恭喜㊗️你，`Husky` 试用成功！！





## Git Hooks 支持的所有钩子见下表（**加粗的为常用钩子**）：

下面这些是操作 `git` 是的钩子脚本，你能在这些钩子脚本中做你想做的事

![v2-161c0a131454d4b24f16a9a3125d947f_1440w](/Users/HuaJi/Downloads/v2-161c0a131454d4b24f16a9a3125d947f_1440w.jpeg)

