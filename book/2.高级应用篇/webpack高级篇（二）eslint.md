@[TOC](目录)
![在这里插入图片描述](https://img-blog.csdnimg.cn/bc6333ff4639496c8bb1e3226f836933.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

`webpack` 结合 `Eslint` 使用

<br />

## 1. 安装 `eslint`

```bash
npx eslint --init
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/37c0b5a4df1f4908b78e4c688034bc3e.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center=500x140)

![在这里插入图片描述](https://img-blog.csdnimg.cn/4416bfe8d70d4b9ab368d468839c8cd9.png#pic_center =500x80)

![在这里插入图片描述](https://img-blog.csdnimg.cn/5e6ee30c45d54c0f9b9ff90b29ff0520.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center =450x100)

![在这里插入图片描述](https://img-blog.csdnimg.cn/7fff805854bd440586030f9b90e51d95.png#pic_center =450x25)

![在这里插入图片描述](https://img-blog.csdnimg.cn/b975dfe3729444cc9403fa5c8277652b.png#pic_center =600x60)

![在这里插入图片描述](https://img-blog.csdnimg.cn/a3ec9edbc0574b65a222f54946f80ed3.png#pic_center =600x70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/3cea2739f973417f97471d2b2f45fde0.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center =600x100)

> - Airbnb: [https://github.com/airbnb/javascript](https://github.com/airbnb/javascript)
> - Standard: [https://github.com/standard/standard](https://github.com/standard/standard)
> - Google: [https://github.com/google/eslint-config-google](https://github.com/google/eslint-config-google)
> - XO: [https://github.com/xojs/eslint-config-xo](https://github.com/xojs/eslint-config-xo)

![在这里插入图片描述](https://img-blog.csdnimg.cn/4ca7e489c88c4685960fcea62267bf01.png#pic_center =400x80)
![在这里插入图片描述](https://img-blog.csdnimg.cn/95c69cd58cc742368668b9efa09b9dbd.png#pic_center =450x22)

安装完成后生成一个elsint配置文件`.eslintrc.js`

```js
module.exports = {
    "env": { // 指定脚本的运行环境。每种环境都有一组特定的预定义全局变量。
        "browser": true, // 运行在浏览器
        "es2021": true // 支持es2021
    },
    "extends": [ // 使用的外部代码格式化配置文件
        "airbnb-base"
    ],
    "parserOptions": {
        "ecmaVersion": 6, // ecmaVersion 用来指定支持的 ECMAScript 版本 。默认为 5，即仅支持
es5
        "sourceType": "module"
    },
  	globals: {}, // 脚本在执行期间访问的额外的全局变量
    "rules": { // 启用的规则及其各自的错误级别。0(off)  1(warning)  2(error)
    }
};
```

**rules:**

https://blog.csdn.net/qq_41887214/article/details/116015157

https://cloud.tencent.com/developer/section/1135684

https://eslint.bootcss.com/docs/rules/
<br />
### 1.1 推荐 eslint-config
[vue 官方: eslint-config-vue](https://github.com/vuejs/eslint-config-vue)
[腾讯 AlloyTeam: eslint-config-alloy](https://alloyteam.github.io/eslint-config-alloy/)

<br />

## 2. Webpack + ESlint 实现实时报错提示

执行`npx eslint ./src/`，可以看到会输出一些error或者waring
![在这里插入图片描述](https://img-blog.csdnimg.cn/994283e357e247119ebf8e071cca654a.png#pic_center)

我们期望`eslint`能够实时提示报错而不必等待执行命令。 这个功能可以通过给自己的IDE(代码编辑器)安装对应的eslint插件来实现。 然

而，不是每个IDE都有插件，如果不想使用插件，又想实时提示报错，那么我们可以结合 webpack 的打包编译功能来实现。

```bash
npm i webpack webpack-cli babel-loader eslint-loader @babel/core webpack-dev-server -D
```

**webpack.config.js**

```js
//... 
module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node-modules/, // 排除编译 node_modules
        use: ['babel-loader', 'eslint-loader']
      },
    ]
  },
// ...
```
或者
```js
{
  test: /\.js$/,
  exclude: /(node_modules)/, // 排除编译 node_modules
  use: [
    {
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-env'],
        plugins: ['@babel/plugin-transform-runtime'],
      },
    },
    {
      loader: 'eslint-loader',
    },
  ],
},
```
执行`npx webpack serve`，如果你遇到下面的错误，那应该是你的`eslint-loader`需要的`eslint`版本没有正确安装
![在这里插入图片描述](https://img-blog.csdnimg.cn/cdc20ff287074a988af37496042c1e94.png#pic_center)

可以执行`npm i`，可以查看到
![在这里插入图片描述](https://img-blog.csdnimg.cn/7a377af68ea949728a8586c1558f8c6c.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

`eslint-loader`需要的`eslint`版本没有正确安装，那么按照他的要求安装即可

```bash
npm i eslint@^7.0.0 -D
```

安装完成后，执行`npx webpack serve`，可以看到`eslint`能够`实时`正确检查代码格式了
![在这里插入图片描述](https://img-blog.csdnimg.cn/ada191f713914e23bb0aa63fb7b8b94b.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

<br />

## 3. 在编辑器中提示

### 3.1 Vscdoe

在插件市场中找到`eslint`插件，安装。重启`vscode`，你可以看到红红的一片代码格式错误￣□￣｜｜
![在这里插入图片描述](https://img-blog.csdnimg.cn/2ee96703635c4032a90300ea7229da35.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/e348812694644d89acd69c2771288c6a.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

<br />

### 3.2 WebStorm
![在这里插入图片描述](https://img-blog.csdnimg.cn/b5a6a35a42684f4d898c3b27aa80e365.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/b639e9f7990745ebaa1cfda6351d8eb2.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

<br />

## 4. 快捷修复

### 4.1 命令行

```bash
npx eslint ./src/ --fix
```

> **优点：**
>
> - 简洁高效
> - 执行完成后能提示未修复的问题及位置

> 缺点：
>
> - 部分问题无法修复，如冗余变量、代码等

<br />

### 4.2 Vscode

[vscode设置eslint保存文件时自动修复eslint错误](https://blog.csdn.net/qq_41887214/article/details/111769188)

启动 vscode 打开项目，

**windows**: `ctrl + shift + p` 打开 搜索面板

**MacBook**: `command + shift + p` 打开 搜索面板

输入 `settings.json`，选择 `.vscode` 下的 `settings.json`

加入以下代码：

```json
{
    // #每次保存的时候将代码按eslint格式进行修复
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
      }
  }
```

 这个规则是在保存时按照你的项目中 eslint 配置 fix

***注意你~/Library/Application Support/Code/User/settings.json 中的规则尽量不要与项目中的规则产生冲突***

<br />

### 4.3 WebStorm

方法1:（不推荐）
![在这里插入图片描述](https://img-blog.csdnimg.cn/db7492606f344f61bb4e8c6c6df15644.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

> **优点：**保存时格式化能够做到实时格式化，避免忘记
>
> **缺点：**保存时修复格式错误可能并不会让你很舒适的coding，因为代码一经格式化，你的代码改变后，你需要重新找到你关注的代码行。这让我不爽



方法2:（推荐）
![在这里插入图片描述](https://img-blog.csdnimg.cn/ee0544f76aef4073ae5e3e1617bc9dd9.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center =400x700)


> **优点：**
>
> - 简洁高效
> - 能够对文件、目录做代码格式修复
>
> **缺点：**
>
> - 1. 部分问题无法修复，如冗余变量、代码等
>   2. 执行完成后不能提示未修复的问题及位置，可以结合

<br />

## 5. .eslintignore

`.eslintignore ` 能够帮助我们忽略我们不想使用 `eslint` 检查的文件、目录，如：

```js
build/*.js
src/assets
public
dist
```

<br />

**`如果有用，就点个赞吧(\*^▽^\*)`**
