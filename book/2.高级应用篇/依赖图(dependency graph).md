# 依赖图(dependency graph)

每当一个文件依赖另一个文件时，webpack 都会将文件视为直接存在 *依赖关系*。这使得 webpack 可以获取非代码资源，如 images 或 web 字体等。并会把它们作为 *依赖* 提供给应用程序。

当 webpack 处理应用程序时，它会根据命令行参数中或配置文件中定义的模块列表开始处理。 从 [*入口*](https://webpack.docschina.org/concepts/entry-points/) 开始，webpack 会递归的构建一个 *依赖关系图*，这个依赖图包含着应用程序中所需的每个模块，然后将所有模块打包为少量的 *bundle* —— 通常只有一个 —— 可由浏览器加载。



> ###### Tip
>
> 对于 *HTTP/1.1* 的应用程序来说，由 webpack 构建的 bundle 非常强大。当浏览器发起请求时，它能最大程度的减少应用的等待时间。而对于 *HTTP/2* 来说，你还可以使用[代码分割](https://webpack.docschina.org/guides/code-splitting/)进行进一步优化。



## bundle分析(bundle analysis)工具

[官方分析工具](https://github.com/webpack/analyse)：是一个不错的开始。还有一些其他社区支持的可选项：

[webpack-chart](https://alexkuz.github.io/webpack-chart/)：webpack stats 可交互饼图。

[webpack-visualizer](https://chrisbateman.github.io/webpack-visualizer/)：可视化并分析你的 bundle，检查哪些模块占用空间，哪些可能是重复使用的。

[webpack-bundle-analyzer](https://github.com/webpack-contrib/webpack-bundle-analyzer)：一个 plugin 和 CLI 工具，它将 bundle 内容展示为一个便捷的、交互式、可缩放的树状图形式。

[webpack bundle optimize helper](https://webpack.jakoblind.no/optimize)：这个工具会分析你的 bundle，并提供可操作的改进措施，以减少 bundle 的大小。

[bundle-stats](https://github.com/relative-ci/bundle-stats)：生成一个 bundle 报告（bundle 大小、资源、模块），并比较不同构建之间的结果。



## webpack-bundle-analyzer

我们来使用 `webpack-bundle-analyzer` 实现。

```bash
# 首先安装这个插件作为开发依赖
# NPM
npm install --save-dev webpack-bundle-analyzer 
# Yarn
yarn add -D webpack-bundle-analyzer
```

```js
const { BundleAnalyzerPlugin } = require('webpack-bundle- analyzer');
module.exports = {
  plugins: [
  // ... 
  new BundleAnalyzerPlugin()]
}
```

执行打包命令，发现控制台里打印出下面这样的日志：

```shell
Webpack Bundle Analyzer is started at http: //127.0.0.1:8888 
Use Ctrl + C to close it
asset bundle.js 5.46 KiB[emitted][minimized](name: main) 1
related asset
asset index.html 352 bytes[emitted]
orphan modules 2.35 KiB[orphan] 3 modules
...
```

我们在浏览器中打开http://127.0.0.1:8888，我们成功可视化了打包产物依赖图！

![image-20211208212952526](/Users/HuaJi/Library/Application Support/typora-user-images/image-20211208212952526.png)

每个文件的占用面积越大则该文件越大，所以找到相关文件优化即可



### options

```js
new BundleAnalyzerPlugin({
  //  可以是`server`，`static`或`disabled`。
  //  在`server`模式下，分析器将启动HTTP服务器来显示软件包报告。
  //  在“静态”模式下，会生成带有报告的单个HTML文件。
  //  在`disabled`模式下，你可以使用这个插件来将`generateStatsFile`设置为`true`来生成Webpack Stats JSON文件。
  analyzerMode: 'server',
  //  将在“服务器”模式下使用的主机启动HTTP服务器。
  analyzerHost: '127.0.0.1',
  //  将在“服务器”模式下使用的端口启动HTTP服务器。
  analyzerPort: 8888, 
  //  路径捆绑，将在`static`模式下生成的报告文件。
  //  相对于捆绑输出目录。
  reportFilename: 'report.html',
  //  模块大小默认显示在报告中。
  //  应该是`stat`，`parsed`或者`gzip`中的一个。
  //  有关更多信息，请参见“定义”一节。
  defaultSizes: 'parsed',
  //  在默认浏览器中自动打开报告
  openAnalyzer: true,
  //  如果为true，则Webpack Stats JSON文件将在bundle输出目录中生成
  generateStatsFile: false, 
  //  如果`generateStatsFile`为`true`，将会生成Webpack Stats JSON文件的名字。
  //  相对于捆绑输出目录。
  statsFilename: 'stats.json',
  //  stats.toJson（）方法的选项。
  //  例如，您可以使用`source：false`选项排除统计文件中模块的来源。
  //  在这里查看更多选项：https：  //github.com/webpack/webpack/blob/webpack-1/lib/Stats.js#L21
  statsOptions: null,
  logLevel: 'info' // 日志级别。可以是'信息'，'警告'，'错误'或'沉默'。
})
```



## 添加 npm 脚本

你可能希望 bundle 分析工具在在指定的命令下才执行，你可以参照 [拆分开发环境和生产环境配置](https://blog.csdn.net/qq_41887214/article/details/121670227) 拆分一个 `report` 环境即可。



