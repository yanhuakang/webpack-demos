> **`为什么要坚持写作？写作就是求甚解的过程。`** 

![在这里插入图片描述](https://img-blog.csdnimg.cn/d1b9056adad746cc82d8b0afac70ea75.gif)
@[TOC](目录)

前面和大家分享过[Git Hooks 与 Husky —— 配合 eslint 规范代码](https://blog.csdn.net/qq_41887214/article/details/121780683)，今天和大家分享【**如何用 commitlint 结合 Husky 来规范团队的 git commit**】

<br />

## 规范 commit 日志的好处
> 1. 团队形成一致的代码提交风格，更好的提高工作效率
> 2. 规范的 commit message 有助于团队其它人员 review, 还可以有效的输出 CHANGELOG, 对项目十分重要
> 3. 成为一名有追求的工程师

<br />


## 安装 & 配置 commitlint

[https://commitlint.js.org/](https://commitlint.js.org/)

<br />

### 安装 `commitlint cli` 和 `config-conventional`

```bash
npm install --save-dev @commitlint/{config-conventional,cli}

# For Windows:
npm install --save-dev @commitlint/config-conventional @commitlint/cli
```

<br />

### 创建 **commitlint.config.js** 配置 `commitlint`

```shell
echo "module.exports = {extends: ['@commitlint/config-conventional']}" > commitlint.config.js
```

<br />

## 安装 & 配置 husky

### 安装 `husky`

```bash
npm install husky --save-dev
```

<br />

### 启用 Git hooks

```bash
npx husky install
```

<br />

### prepare 脚本

执行下面的命令，会在 **packgae.json** 中添加 `prepare` 脚本

```bash
npm set-script prepare "husky install"
```

执行上面的命令我们会得到如下配置， `prepare` 脚本会在执行 `npm install` 后会自动执 `husky install` 命令，该命令会创建 `.husky/` 目录并指定该目录为 `git hooks` 所在的目录。

```json
{
  "scripts": {
    "prepare": "husky install"
  }
}
```

<br />

### 添加 `commit-msg` hook

运行以下命令创建 `commit-msg` hook

```bash
npx husky add .husky/commit-msg 'npx --no -- commitlint --edit "$1"'

# 有时上面的命令在某些命令解释器中不起作用
# 你可以尝试下面的命令在commit-msg文件中写入 npx --no -- commitlint --edit $1
npx husky add .husky/commit-msg \"npx --no -- commitlint --edit '$1'\"
# or
npx husky add .husky/commit-msg "npx --no -- commitlint --edit $1"
```


提交 commit，发现 `commitlint` 已经生效，不符合 commitlint 规范的 `git commit` 的将被停止



> **Tip**
>
> 如果你遇到如下报错，删除node_modules并执行 `npm install` 重新安装即可
>
> ```
> .husky/_/husky.sh: No such file or directory
> ```

<br />

## commitlint 规范

### 提交格式

```bash
git commit -m <type>(scope?): <subject>
```



#### Examples

```bash
git commit -m 'chore: run tests on travis ci'

git commit -m 'fix(server): send cors headers'

git commit -m 'feat(blog): add comment section'
```

> 注意，英文冒号 + 空格

<br />

### 常用的type类别

**type**：用于表明我们这次提交的改动类型，是新增了功能？还是修改了测试代码？又或者是更新了文档？总结以下 11 种类型：

> - **build**：主要目的是修改项目构建系统(例如 glup，webpack，rollup 的配置等)的提交
> - **ci**：主要目的是修改项目继续集成流程(例如 Travis，Jenkins，GitLab CI，Circle等)的提交
> - **docs**：文档更新
> - **feat**：新增功能
> - **fix**：bug 修复
> - **perf**：性能优化
> - **refactor**：重构代码(既没有新增功能，也没有修复 bug)
> - **revert**：回滚某个更早之前的提交
> - **style**：不影响程序逻辑的代码修改(修改空白字符，补全缺失的分号等)
> - **test**：新增测试用例或是更新现有测试
> - **chore**：不属于以上类型的其他类型(日常事务)

更多的 type 说明 你可以参阅这里 [commitlint type-enum](https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-conventional#type-enum)



**scope**：作用域，可选。用于标识此次提交主要涉及到代码中哪些模块。支持多作用域（可使用分隔符："/"、"\\" 和 ","

**subject**：一句话描述此次提交的主要内容，做到言简意赅。


<br />


## 自定义 commitlint 规范

[https://commitlint.js.org/#/reference-rules](https://commitlint.js.org/#/reference-rules)

![在这里插入图片描述](https://img-blog.csdnimg.cn/d6fbe7eb1994403ba82e50e3fc914bbb.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16)


我们可以通过修改 **commitlint.config.js** 来自定义我们的提交规范，如：

`rule`配置说明：`rule`由`name`和配置数组组成，如：`'name: [0, 'always', 72]'`，数组中第一位为`level`，可选0,1,2，0为disable，1为warning，2为error；第二位为是否启用，可选`always|never`，第三位该rule的值。具体配置例子如下：

```js
// commitlint.config.js
module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'type-enum': [2, 'always', [
      'test', 'upd', 'feat', 'fix', 'refactor', 'docs', 'chore', 'style', 'revert',
    ]],
    'type-case': [0],
    'type-empty': [0],
    'scope-empty': [0],
    'scope-case': [0],
    'subject-full-stop': [0, 'never'],
    'subject-case': [0, 'never'],
    'header-max-length': [0, 'always', 72],
  },
};
```

<br />

## 使用commitlint好处
> 我们都知道，在使用 git commit 时，git 会提示我们填入此次提交的信息。
> 可不要小看了这些 commit，团队中规范了 commit 可以更清晰的查看每一次代码提交记录，还可以根据自定义的规则，自动生成 changeLog 文件。

- [Why Use Conventional Commits?](https://www.conventionalcommits.org/en/v1.0.0-beta.2/#why-use-conventional-commits)

- ["The perks of committing with conventions" (Talk slides)](https://slides.com/marionebl/the-perks-of-committing-with-conventions#/)

<br />

## 参阅
- [https://commitlint.js.org/](https://commitlint.js.org/)
- [https://typicode.github.io/husky/#/?id=install](https://typicode.github.io/husky/#/?id=install)
- [https://github.com/conventional-changelog/commitlint](https://github.com/conventional-changelog/commitlint)
- [https://github.com/conventional-changelog/commitlint/blob/master/@commitlint/config-conventional/index.js](https://github.com/conventional-changelog/commitlint/blob/master/@commitlint/config-conventional/index.js)
- [https://www.conventionalcommits.org/en/v1.0.0-beta.2/#why-use-conventional-commits](https://www.conventionalcommits.org/en/v1.0.0-beta.2/#why-use-conventional-commits)