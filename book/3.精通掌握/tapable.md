# Tapable



## tapable 简介

这个小型库是 webpack 的一个核心工具，但也可用于其他地方， 以提供类似的插件接口。 在 webpack 中的许多对象都扩展自 `Tapable` 类。 它对外暴露了 `tap`，`tapAsync` 和 `tapPromise` 等方法， 插件可以使用这些方法向 webpack 中注入自定义构建的步骤，这些步骤将在构建过程中触发。



<br />



### 参数

所有Hook构造函数都有一个可选参数，这是一个定义形参的字符串列表。

```js
const hook = new SyncHook(["arg1", "arg2", "arg3"]);
```



<br />



### Hooks 分类

```js
const {
  // 同步执行
  SyncHook,
  SyncBailHook,
  SyncWaterfallHook,
  SyncLoopHook,
  
  // 异步并行执行
  AsyncParallelHook,
  AsyncParallelBailHook,
  
  // 异步串行执行
  AsyncSeriesHook,
  AsyncSeriesBailHook,
  AsyncSeriesWaterfallHook
} = require("tapable");
```



| 名称                     | 解释               |
| ------------------------ | ------------------ |
| SyncHook                 | 同步钩子           |
| SyncBailHook             | 同步熔断钩子       |
| SyncWaterfallHook        | 同步瀑布流钩子     |
| SyncLoopHook             | 同步循环钩子       |
| AsyncParallelHook        | 异步并行钩子       |
| AsyncParallelBailHook    | 异步并行熔断钩子   |
| AsyncSeriesHook          | 异步串行钩子       |
| AsyncSeriesBailHook      | 异步串行熔断钩子   |
| AsyncSeriesWaterfallHook | 异步串行瀑布流钩子 |



<br />



### 绑定方式



> - 同步的钩子要用 `tap` 方法来绑定
>
> - 异步的钩子可以像同步方式一样用 `tap` 方法来绑定，也可以用 `tapAsync` 或 `tapPromise` 这两个异步方法来绑定。
>
>   - tapAsync
>
>     当我们用 `tapAsync` 方法来绑定hook时，_必须_调用函数的最后一个参数 `callback` 指定的回调函数。
>
>   - tapPromise
>
>     当我们用 `tapPromise` 方法来绑定hook时，_必须_返回一个 pormise ，异步任务完成后 resolve 。



<br />



### 调用方式

> - 同步的钩子要用 `call` 方法来绑定
>
> - 异步的钩子需要用 `callAsync` 或 `promise` 这两个异步方法来调用
>
>   - callAsync
>
>     当我们用 `callAsync` 方法来调用hook时，第二个参数需要时一个回调函数，参数是执行任务的最后一个返回值
>
>   - promise
>
>     当我们用 `promise` 方法来调用hook时，需要使用 then 来处理执行结果，参数是执行任务的最后一个返回值。



<br />



## SyncHook

同步钩子

```js
const { SyncHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat: new SyncHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tap('Animal', (food) => {
      console.log('eat1-1', food);
    });
    this.hooks.eat.tap('Animal', (food) => {
      console.log('eat1-2', food);
    });
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.call(params);
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
```



运行结果

```
eat1-1 鱼🐟
eat1-2 鱼🐟
```





<br />



## SyncBailHook

`SyncBailHook` 有返回值时，会终止当前 SyncBailHook 实例对象的继续调用



```js
const { SyncHook, SyncBailHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat1: new SyncHook(['food']),
      eat2: new SyncBailHook(['food']),
      eat3: new SyncBailHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat2.tap('Animal', (food) => {
      console.log('eat2-1', food);
      return 1; //
    });
    this.hooks.eat2.tap('Animal', (food) => {
      console.log('eat2-2', food);
    });

    this.hooks.eat3.tap('Animal', (food) => {
      console.log('eat3', food);
    });

    this.hooks.eat1.tap('Animal', (food) => {
      console.log('eat1', food);
    });
  }

  // 调用钩子函数
  start(params) {
    // 调用顺序会影响钩子函数的执行顺序
    // 这也是webpack compiler hooks钩子不同阶段执行不同钩子的原理
    this.hooks.eat2.call(params);
    this.hooks.eat3.call(params);
    this.hooks.eat1.call(params);
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
```



输出

```
eat2-1 鱼🐟
eat3 鱼🐟
eat1 鱼🐟
```

可以看到 eat2-1 的返回值，而 eat2-2 没有执行，但eat3、eat1却执行了。

这说明 `SyncBailHook` 有返回值时，只会终止当前 SyncBailHook 实例对象的继续调用，不会影响其他实例对象的调用





<br />



## SyncWaterfallHook

同步瀑布钩子函数，返回值会被后面的相同实例钩子函数接收

```js
const { SyncWaterfallHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat: new SyncWaterfallHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tap('Animal', (food) => {
      console.log('eat1-1', food);
      return '老鼠🐭';
    });
    this.hooks.eat.tap('Animal', (food) => {
      console.log('eat1-2', food);
    });
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.call(params);
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
```



执行结果

```
eat1-1 鱼🐟
eat1-2 老鼠🐭
```



<br />





## SyncLoopHook

- SyncLoopHook 的特点是不停的循环执行回调函数，直到函数结果等于 undefined
- 要注意的是每次循环都是从头开始循环的

```js
const { SyncLoopHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat: new SyncLoopHook(['food']),
    };
    this.index = 0;
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tap('Animal', (food) => {
      console.log('eat1-1', food);
      return ++this.index === 3 ? undefined : '继续';
    });
    this.hooks.eat.tap('Animal', (food) => {
      console.log('eat1-2', food);
    });
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.call(params);
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
```



执行结果

```
eat1-1 鱼🐟
eat1-1 鱼🐟
eat1-1 鱼🐟
eat1-2 鱼🐟
```





<br />



## AsyncParallelHook

异步并行执行钩子

```js
const { AsyncParallelHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat: new AsyncParallelHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tapAsync('Animal', (food, callback) => {
      setTimeout(() => {
        console.log('eat1-1', food);
        callback();
      }, 2000);
    });
    this.hooks.eat.tapPromise('Animal', (food) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('eat1-2', food);
        resolve();
      }, 1000);
    }));
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.callAsync(params, () => {
      console.log('end');
    });
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
```



执行结果：

1. 一秒后打印 “eat1-2 鱼🐟”
2. 再过一秒打印 “eat1-1 鱼🐟”
3. 两个钩子执行结束都调用了callback 或者 resolve，打印 “end”

```
eat1-2 鱼🐟
eat1-1 鱼🐟
end
```





<br />



## AsyncParallelBailHook

- 异步并行执行钩子
- 有一个任务返回值不为空就直接结束

```js
const { AsyncParallelBailHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat: new AsyncParallelBailHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tapPromise('Animal', (food) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('eat1-1', food);
        resolve(); // 此处为 undefined 进入到下一个回调
      }, 1000);
    }));
    this.hooks.eat.tapPromise('Animal', (food) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('eat1-2', food);
        resolve('eat1-2'); // 这个回调完成后直接触发最后回调
      }, 1000);
    }));
    this.hooks.eat.tapPromise('Animal', (food) => new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log('eat1-3', food);
        reject('eat1-3');
      }, 3000);
    }));
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.promise(params).then(() => {
      console.log('end');
    }).catch((error) => {
      console.log('error', error);
    });
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
```



执行结果

```
eat1-2 鱼🐟
eat1-1 鱼🐟
end
eat1-3 鱼🐟
```





<br />



## AsyncSeriesHook

- 异步串行钩子
- 任务一个一个执行，执行完上一个执行下一个

```js
const { AsyncSeriesHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat: new AsyncSeriesHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tapAsync('Animal', (food, callback) => {
      setTimeout(() => {
        console.log('eat1-1', food);
        callback();
      }, 2000);
    });
    this.hooks.eat.tapPromise('Animal', (food) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('eat1-2', food);
        resolve();
      }, 1000);
    }));
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.callAsync(params, () => {
      console.log('end');
    });
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
```



执行结果

```
eat1-1 鱼🐟
eat1-2 鱼🐟
end
```





<br />



## AsyncSeriesBailHook

- 异步串行熔断钩子
- 只要有一个返回了不为 undefiend 的值就直接结束

```js
const { AsyncSeriesBailHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat: new AsyncSeriesBailHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tapAsync('Animal', (food, callback) => {
      setTimeout(() => {
        console.log('eat1-1', food);
        callback('eat1-1'); // 这个回调完成后直接触发最后回调
      }, 2000);
    });
    this.hooks.eat.tapPromise('Animal', (food) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('eat1-2', food);
        resolve();
      }, 1000);
    }));
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.callAsync(params, () => {
      console.log('end');
    });
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
```





执行结果

```
eat1-1 鱼🐟
end
```





<br />



## AsyncSeriesWaterfallHook

- 异步串行瀑布流钩子
- 任务一个一个执行，执行完上一个执行下一个
- 上一个钩子返回结果会作为下一个钩子回调函数的参数

```js
const { AsyncSeriesWaterfallHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat: new AsyncSeriesWaterfallHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tapPromise('Animal', (food) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('eat1-1', food);
        resolve(); // 此处为 undefined 进入到下一个回调
      }, 2000);
    }));
    this.hooks.eat.tapPromise('Animal', (food) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('eat1-2', food);
        resolve('老鼠🐭'); // 这个回调完成后直接触发最后回调
      }, 1000);
    }));
    this.hooks.eat.tapPromise('Animal', (food) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('eat1-3', food);
        resolve('eat1-3');
      }, 3000);
    }));
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.promise(params).then(() => {
      console.log('end');
    }).catch((error) => {
      console.log('error', error);
    });
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
```



执行结果

```
eat1-1 鱼🐟
eat1-2 鱼🐟
eat1-3 老鼠🐭
end
```



源码：[https://gitee.com/yanhuakang/webpack-demos/tree/master/proficient/step_2-tapable/test](https://gitee.com/yanhuakang/webpack-demos/tree/master/proficient/step_2-tapable/test)