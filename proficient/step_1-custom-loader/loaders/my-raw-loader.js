// eslint-disable-next-line import/no-extraneous-dependencies
const { validate } = require('schema-utils');

const schema = {
  type: 'object',
  properties: {
    esModule: {
      type: 'boolean',
    },
  },
  additionalProperties: false, // 是否允许不存在的选项传入
};

function myRawLoader(source) {
  // https://webpack.docschina.org/contribute/writing-a-loader/
  // https://webpack.docschina.org/api/loaders/
  const options = this.getOptions();
  validate(schema, options, {
    name: 'my-raw-loader',
    baseDataPath: 'options',
  });

  // 提取给定的 loader 选项，
  // 从 webpack 5 开始，this.getOptions 可以获取到 loader 上下文对象。它用来替代来自 loader-utils 中的 getOptions 方法。
  console.log('esModule:', options.esModule);
  if (!options.esModule) {
    return `module.exports = ${JSON.stringify(source)}`;
  }
  return `export default ${JSON.stringify(source)}`;
}

module.exports = myRawLoader;
