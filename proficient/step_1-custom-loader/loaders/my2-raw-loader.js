// 用来验证my2-raw-loader接收的source是my-raw-loader的返回值源内容
function my2RawLoader(source) {
  // eslint-disable-next-line no-eval
  const str = eval(source);
  console.log('my2RawLoader source', source);
  console.log('my2RawLoader str', str);

  return source;
}

module.exports = my2RawLoader;
