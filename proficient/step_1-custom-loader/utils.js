function tplReplace(template, replaceData) {
  return template.replace(/\{\{(.*?)}}/g, (word, key) => replaceData[key]);
}
module.exports = {
  tplReplace,
};
