// eslint-disable-next-line import/no-extraneous-dependencies
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  mode: 'development',
  resolveLoader: {
    modules: ['node_modules', path.resolve(__dirname, 'loaders')],
  },
  module: {
    rules: [
      {
        test: /\.js/,
        use: './loaders/my-test.js', // 测试 content 中是否包含注释代码；结果：包含。
      },
      {
        test: /.txt$/,
        use: [
          'my2-raw-loader', // 用来验证my2-raw-loader接收的source是my-raw-loader的返回值源内容
          {
            loader: 'my-raw-loader',
            options: {
              esModule: false,
            },
          },
        ],
      },
      {
        test: /.tpl$/,
        use: [
          {
            loader: 'tpl-loader',
            options: {
              log: false,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: '自定义 webpack loader',
      template: './src/index.html',
    }),
  ],
};
