const { AsyncSeriesBailHook, SyncHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat0: new SyncHook(['food']),
      eat: new AsyncSeriesBailHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tapAsync('Animal', (food, callback) => {
      setTimeout(() => {
        console.log('eat1-1', food);
        callback('eat1-1'); // 这个回调完成后直接触发最后回调
      }, 2000);
    });
    this.hooks.eat.tapPromise('Animal', (food) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('eat1-2', food);
        resolve();
      }, 1000);
    }));
    this.hooks.eat0.tap('Animal', (food) => {
      console.log('eat0-1', food);
    });
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.callAsync(params, () => {
      console.log('end');
    });
    this.hooks.eat0.call(params);
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
