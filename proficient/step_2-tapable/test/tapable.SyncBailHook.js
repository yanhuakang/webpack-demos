const { SyncHook, SyncBailHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat1: new SyncHook(['food']),
      eat2: new SyncBailHook(['food']),
      eat3: new SyncBailHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat2.tap('Animal', (food) => {
      console.log('eat2-1', food);
      return 1; //
    });
    this.hooks.eat2.tap('Animal', (food) => {
      console.log('eat2-2', food);
    });

    this.hooks.eat3.tap('Animal', (food) => {
      console.log('eat3', food);
    });

    this.hooks.eat1.tap('Animal', (food) => {
      console.log('eat1', food);
    });
  }

  // 调用钩子函数
  start(params) {
    // 调用顺序会影响钩子函数的执行顺序
    // 这也是webpack compiler hooks钩子不同阶段执行不同钩子的原理
    this.hooks.eat2.call(params);
    this.hooks.eat3.call(params);
    this.hooks.eat1.call(params);
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
