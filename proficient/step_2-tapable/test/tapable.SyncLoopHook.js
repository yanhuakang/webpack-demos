const { SyncLoopHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat: new SyncLoopHook(['food']),
    };
    this.index = 0;
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tap('Animal', (food) => {
      console.log('eat1-1', food);
      // eslint-disable-next-line no-plusplus
      return ++this.index === 3 ? undefined : '继续';
    });
    this.hooks.eat.tap('Animal', (food) => {
      console.log('eat1-2', food);
    });
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.call(params);
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
