const { SyncWaterfallHook } = require('tapable');

class Animal {
  constructor() {
    this.hooks = {
      eat: new SyncWaterfallHook(['food']),
    };
  }

  // 绑定钩子函数
  tap() {
    this.hooks.eat.tap('Animal', (food) => {
      console.log('eat1-1', food);
      return '老鼠🐭';
    });
    this.hooks.eat.tap('Animal', (food) => {
      console.log('eat1-2', food);
    });
  }

  // 调用钩子函数
  start(params) {
    this.hooks.eat.call(params);
  }
}

const cat = new Animal();

cat.tap();
cat.start('鱼🐟');
