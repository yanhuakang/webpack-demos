/* eslint-disable */

const {
  SyncHook,
  SyncBailHook,
  SyncWaterfallHook,
  SyncLoopHook,
  AsyncParallelHook,
  AsyncParallelBailHook,
  AsyncSeriesHook,
  AsyncSeriesBailHook,
  AsyncSeriesWaterfallHook,
} = require('tapable');

class Lesson {
  constructor() {
    this.hooks = {
      go: new SyncHook(['address']), // ['address']是定义回调函数的形参
      go2: new SyncBailHook(['address']), // return 终止后续当前SyncBailHook实例化的方法，即 go2 的return只会影响go2，不会影响 go 和 go3
      go3: new SyncBailHook(['address']), // return 终止后续当前SyncBailHook实例化的方法
      go4: new AsyncParallelHook(['name', 'age']), // 异步并行
      go5: new AsyncSeriesHook(['name', 'age']), // 异步串行，只影响当前AsyncSeriesHook实例对象的串行执行
      go6: new AsyncSeriesHook(['name', 'age']), // 异步串行
    };
  }

  tap() {
    this.hooks.go.tap('go1-1', (address) => {
      console.log('go1-1', address);
    });

    this.hooks.go2.tap('go2-1', (address) => {
      console.log('go2-1', address);
      return 2;
    });
    this.hooks.go3.tap('go3-1', (address) => {
      console.log('go3-1', address);
      return 2;
    });

    this.hooks.go2.tap('go2-2', (address) => {
      console.log('go2-2', address);
      return 1;
    });

    this.hooks.go4.tapAsync('go4-1', (name, age, cb) => {
      setTimeout(() => {
        console.log('go4-1', name, age);
        cb();
      }, 2000);
    });

    this.hooks.go4.tapPromise('go4-2', (name, age) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('go4-2', name, age);
        resolve();
      }, 1000);
    }));

    this.hooks.go5.tapAsync('go5-1', (name, age, cb) => {
      setTimeout(() => {
        console.log('go5-1', name, age);
        cb();
      }, 2000);
    });

    this.hooks.go5.tapPromise('go5-2', (name, age) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('go5-2', name, age);
        resolve();
      }, 1000);
    }));

    this.hooks.go6.tapPromise('go6-1', (name, age) => new Promise((resolve) => {
      setTimeout(() => {
        console.log('go6-1', name, age);
        resolve();
      }, 1000);
    }));

    this.hooks.go6.tapAsync('go6-2', (name, age, cb) => {
      setTimeout(() => {
        console.log('go6-2', name, age);
        cb();
      }, 1000);
    });

    this.hooks.go.tap('go1-2', (address) => {
      console.log('go1-2', address);
    });
  }

  start(address) {
    this.hooks.go.call(address);
    this.hooks.go2.call(address);
    this.hooks.go3.call(address);
    this.hooks.go4.callAsync('jack', 12, () => {});
    this.hooks.go5.callAsync('jack2', 18, () => {});
    this.hooks.go6.callAsync('jack3', 19, () => {});
  }
}

const l = new Lesson();
l.tap();
l.start('1');
