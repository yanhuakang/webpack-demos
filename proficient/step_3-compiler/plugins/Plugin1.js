class Plugin1 {
  // eslint-disable-next-line class-methods-use-this
  apply(compiler) {
    compiler.hooks.emit.tap('Plugin1', () => {
      console.log('emit Plugin1');
    });
    compiler.hooks.emit.tapAsync('Plugin1', (compilation, cb) => {
      setTimeout(() => {
        console.log('emit tapAsync Plugin1');
        cb();
      }, 3000);
    });
    compiler.hooks.afterEmit.tapAsync('Plugin1', (compilation, cb) => {
      setTimeout(() => {
        console.log('afterEmit tapAsync Plugin1');
        cb();
      }, 1000);
    });
    compiler.hooks.done.tap('Plugin1', () => {
      console.log('done Plugin1');
    });
  }
}

module.exports = Plugin1;
