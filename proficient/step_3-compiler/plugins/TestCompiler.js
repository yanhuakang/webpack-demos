class TestCompiler {
  // eslint-disable-next-line class-methods-use-this
  apply(compiler) {
    compiler.hooks.entryOption.tap('TestCompiler', (context, entry) => {
      console.log('entryOption', context, entry);
    });

    compiler.hooks.afterPlugins.tap('TestCompiler', (context, entry) => {
      console.log('afterPlugins', context, entry);
    });
  }
}

module.exports = TestCompiler;
