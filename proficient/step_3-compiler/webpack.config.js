const Plugin1 = require('./plugins/Plugin1');
const TestCompiler = require('./plugins/TestCompiler');

module.exports = {
  mode: 'development',
  entry: {
    main: './src/index.js',
    main2: './src/a.js',
  },
  plugins: [
    new Plugin1(),
    new TestCompiler(),
  ],
};
