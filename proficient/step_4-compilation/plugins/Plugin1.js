class Plugin1 {
  // eslint-disable-next-line class-methods-use-this
  apply(compiler) {
    compiler.hooks.thisCompilation.tap('Plugin1', (compilation) => {
      compilation.hooks.additionalAssets.tapAsync('Plugin1', (callback) => {
        console.log('compilation', compilation);
        const content = 'hello compilation';
        // 方式1：
        // compilation.assets['a.txt'] = {
        //   size: () => content.length,
        //   source: () => content,
        // };

        // 方式2：
        // webpack 模块实例，可以通过 compiler 对象访问，
        // 这样确保使用的是模块的正确版本
        // （不要直接 require/import webpack）
        const { webpack } = compiler;

        // RawSource 是其中一种 “源码”("sources") 类型，
        // 用来在 compilation 中表示资源的源码
        const { RawSource } = webpack.sources;

        // 向 compilation 添加新的资源，
        // 这样 webpack 就会自动生成并输出到 output 目录
        compilation.emitAsset('a.txt', new RawSource(content));

        callback();
      });
    });
  }
}

module.exports = Plugin1;
