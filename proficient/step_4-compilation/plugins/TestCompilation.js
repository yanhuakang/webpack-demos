class TestCompilation {
  // eslint-disable-next-line class-methods-use-this
  apply(compiler) {
    compiler.hooks.thisCompilation.tap('TestCompilation', (compilation) => {
      compilation.hooks.buildModule.tap('TestCompilation', (module) => {
        console.log('module', module);
      });
      compilation.hooks.chunkIds.tap('TestCompilation', (chunks) => {
        console.log('beforeChunkIds', chunks);
      });
    });
  }
}

module.exports = TestCompilation;
