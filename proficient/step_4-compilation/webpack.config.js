const Plugin1 = require('./plugins/Plugin1');
const TestCompilation = require('./plugins/TestCompilation');

module.exports = {
  mode: 'production',
  plugins: [
    new Plugin1(),
    new TestCompilation(),
  ],
};
