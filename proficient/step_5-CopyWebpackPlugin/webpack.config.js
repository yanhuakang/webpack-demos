const CopyWebpackPlugin = require('./plugin/CopyWebpackPlugin');

module.exports = {
  mode: 'production',

  output: {
    clean: true,
  },

  plugins: [
    new CopyWebpackPlugin({
      form: 'public',
      to: 'css',
      ignore: ['**/index.html'],
    }),
  ],
};
