// eslint-disable-next-line import/no-extraneous-dependencies
const loaderUtils = require('loader-utils');

function fileLoader(content) {
  // 1. 根据文件内容生成带 hash 值文件名
  const filename = loaderUtils.interpolateName(this, '', {
    content,
  });
  // 2. 将文件输出出去
  this.emitFile(filename, content);

  // 3. 返回 export default "[contenthash].[ext]"
  return `export default "${filename}"`;
}
// file-loader 需要处理图片、字体等文件，它们都是buffer数据，需要使用 raw loader 才能处理
fileLoader.raw = true;

module.exports = fileLoader;
