// eslint-disable-next-line import/no-extraneous-dependencies
const { validate } = require('schema-utils');

const schema = {
  type: 'object',
  properties: {
    esModule: {
      type: 'boolean',
    },
  },
  additionalProperties: false,
};
function myRawLoader(content, map, meta) {
  const options = this.getOptions();

  validate(schema, options, {
    name: 'my-raw-loader',
    baseDataPath: 'options',
  });

  console.log('options', options);
  console.log('content', content);
  console.log('map', map);
  console.log('meta', meta);
  if (options.esModule) {
    return `export default ${JSON.stringify(content)}`;
  }
  return `module.exports = ${JSON.stringify(content)}`;
}
module.exports = myRawLoader;
