function styleLoader() {}

styleLoader.pitch = function (remainingRequest) {
  // remainingRequest：剩下还需要处理的 loader
  console.log('remainingRequest：', remainingRequest);
  // eslint-disable-next-line max-len
  // remainingRequest：/Users/huaji/learning/code/webpack-demos/node_modules/css-loader/dist/cjs.js!/Users/huaji/learning/code/webpack-demos/proficient/step_6-loaders/node_modules/less-loader/dist/cjs.js!/Users/huaji/learning/code/webpack-demos/proficient/step_6-loaders/src/testStyleLoader/index.less

  // 1. 将`remainingRequest`中的绝对路径转换为相对路径
  // https://webpack.docschina.org/api/loaders/#thisutils
  const relativePath = remainingRequest.split('!').map((absolutePath) => this.utils.contextify(this.context, absolutePath)).join('!');
  console.log('relativePath：', relativePath);
  // eslint-disable-next-line max-len
  // relativePath：../../../../node_modules/css-loader/dist/cjs.js!../../node_modules/less-loader/dist/cjs.js!./index.less

  return `
    // 2. 引入 less-loader、css-loader 处理后的结果
    import style from "!!${relativePath}";
    // 3. 动态生成\`style\`标签插入到页面
    const styleDom = document.createElement('style');
    styleDom.innerHTML = style;
    document.head.appendChild(styleDom);
  `;
};

module.exports = styleLoader;

// eslint-disable-next-line max-len
// 1. `relativePath` ../../../../node_modules/css-loader/dist/cjs.js!../../node_modules/less-loader/dist/cjs.js!./index.less
//    `relativePath`是`inline loader`用法，代表`./index.less`需要使用`less-loader`和`css-loader`处理
// 2. import style from "!!${relativeRequest}"
//    !! 表示跳过 pre、 normal 和 post loader。
