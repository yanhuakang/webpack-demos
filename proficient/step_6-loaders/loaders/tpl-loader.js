const schema = {
  type: 'object',
  properties: {
    log: {
      type: 'boolean',
    },
  },
  additionalProperties: false,
};

function tplReplace(template, replaceData) {
  // 正则匹配{{}}替换为值
  return template.replace(/\{\{(.*?)}}/g, (word, key) => replaceData[key]);
}

function tplLoader(content) {
  const options = this.getOptions(schema);
  // 去除模板中的不可见字符
  const resource = content.replace(/\s+/g, '');
  const logStr = options.log ? `console.log('Compiled the file from ${this.resourcePath}')` : '';
  // 返回 esm export 及 函数
  // 函数接收外部参数，函数内部声明一个函数，然后调用
  // 调用时注意：第一个参数 `template` 应当是一个字符串，切记
  return `export default (data) => {
    ${tplReplace.toString()}
    ${logStr}
    return tplReplace('${resource}', data)
  }`;
}
module.exports = tplLoader;
