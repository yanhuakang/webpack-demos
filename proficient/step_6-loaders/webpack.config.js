const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  mode: 'development',
  output: {
    clean: true,
  },
  devServer: {
    port: 9000,
    hot: true,
    open: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/index.html'),
    }),
  ],

  module: {
    rules: [
      {
        test: /\.txt$/i,
        use: [
          {
            loader: path.resolve(__dirname, './loaders/my-raw-loader.js'),
            options: {
              esModule: true,
            },
          },
        ],
      },
      {
        test: /\.tpl$/i,
        use: [
          {
            loader: path.resolve(__dirname, './loaders/tpl-loader.js'),
            options: {
              log: true,
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: path.resolve(__dirname, './loaders/file-loader.js'),
      },
      {
        test: /\.(css|less)$/i,
        use: [path.resolve(__dirname, './loaders/style-loader.js'),
          'css-loader',
          'less-loader',
        ],
      },
    ],
  },
};
