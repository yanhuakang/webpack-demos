class CleanWebpackPlugin {
  apply(compiler) {
    // 获取操作文件的对象
    const fs = compiler.outputFileSystem;

    // 绑定到 “emit” 钩子，在新的打包产物输出前进行clean
    compiler.hooks.emit.tapAsync('CleanWebpackPlugin', (compilation, callback) => {
      // 获取输出文件目录
      const outputPath = compiler.options.output.path;

      // 删除目录所有文件
      this.removeFiles(fs, outputPath);
      callback();
    });
  }

  removeFiles(fs, filePath) {
    console.log('filePath', filePath);
    // 读取当前目录下所有文件
    const exists = fs.existsSync(filePath);
    console.log('exists', exists);
    const files = fs.existsSync(filePath) ? fs.readdirSync(filePath) : [];

    // 递归删除
    files.forEach((file) => {
      // 获取文件完整路径
      const path = `${filePath}/${file}`;
      // 判断是否是文件夹
      if (fs.statSync(path).isDirectory()) {
        // 是文件夹则需要递归
        this.removeFiles(fs, path);
      } else {
        // 是文件则要直接删除，且是同步删除
        fs.unlinkSync(path);
      }
    });

    if (exists) {
      // 最后删除当前目录
      fs.rmdirSync(filePath);
    }
  }
}

module.exports = CleanWebpackPlugin;
