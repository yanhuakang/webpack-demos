const CleanWebpackPlugin = require('./plugin/CleanWebpackPlugin');

module.exports = {
  mode: 'production',

  output: {
    // clean: true,
  },

  plugins: [
    new CleanWebpackPlugin(),
  ],
};
