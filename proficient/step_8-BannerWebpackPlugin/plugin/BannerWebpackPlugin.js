const { validate } = require('schema-utils');

const schema = {
  type: 'object',
  properties: {
    author: {
      type: 'string',
    },
    ext: {
      type: 'array',
    },
  },
  additionalProperties: false, // 是否允许不存在的选项传入
};

class BannerWebpackPlugin {
  constructor(options = {}) {
    // 校验插件options
    validate(schema, options, {
      name: 'BannerWebpackPlugin',
      baseDataPath: 'options',
    });
    this.bannerExt = options.ext || ['js'];
    this.author = options.author;
  }

  apply(compiler) {
    // 绑定到 “emit” 钩子，在新的打包产物输出前进行
    compiler.hooks.emit.tapAsync('BannerWebpackPlugin', (compilation, callback) => {
      // 获取输出文件目录
      const { assets } = compilation;
      const paths = Object.keys(assets).filter((filePath) => {
        const arr = filePath.split('.');
        const ext = arr[arr.length - 1];
        return this.bannerExt.includes(ext);
      });
      console.log('paths', paths);

      paths.forEach((filePath) => {
        const asset = assets[filePath];
        const source = `
/*
  Author：${this.author}
*/
${asset.source()}
`;

        // 向 compilation 添加新的资源，使用`compilation.emitAsset`，但此处为已有资源，所以使用`compilation.assets`覆盖资源
        // 覆盖资源
        // eslint-disable-next-line no-param-reassign
        compilation.assets[filePath] = {
          // 资源内容
          source() {
            return source;
          },
          // 资源大小
          size() {
            return source.length;
          },
        };
      });

      callback();
    });
  }
}

module.exports = BannerWebpackPlugin;
