// eslint-disable-next-line import/no-extraneous-dependencies
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BannerWebpackPlugin = require('./plugin/BannerWebpackPlugin');

module.exports = {
  mode: 'production',

  output: {
    clean: true,
  },

  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.txt$/i,
        type: 'asset/source',
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        type: 'asset/resource',
      },
    ],
  },

  plugins: [
    new MiniCssExtractPlugin(),
    new BannerWebpackPlugin({
      author: '雁行',
      ext: ['js', 'css'],
    }),
  ],
};
